import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AuthService } from './shared/services/auth.service';
import { AuthServiceMock } from './shared/services/auth.service.spec';
import { LocalStorageService } from './shared/services/local-storage.service';
import { LocalStorageServiceMock } from './shared/services/local-storage.service.spec';
import { UserService } from './shared/services/user.service';
import { UserServiceMock } from './shared/services/user.service.spec';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: LocalStorageService, useClass: LocalStorageServiceMock },
        { provide: UserService, useClass: UserServiceMock },
      ],
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  // it('should create the app', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.componentInstance;
  //   expect(app).toBeTruthy();
  // });
});
