import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormService } from 'src/app/shared/services/form.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordPage implements OnInit {
  form: FormGroup;
  loading: boolean;
  formSubmitted: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private formService: FormService,
    private router: Router,
    private authService: AuthService
  ) {
    this.createForm();
  }

  ngOnInit() {}

  createForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
    });
  }

  async onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      const email: string = this.form.get('email').value;
      try {
        this.loading = true;
        await this.authService.forgotPassword(email);
        this.formSubmitted = true;
      } catch (err) {}

      this.loading = false;
    }
  }

  async goto(route: string) {
    await this.router.navigate([route]);
  }
}
