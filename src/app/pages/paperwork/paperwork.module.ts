import { NgModule } from '@angular/core';
import { PaperworkComponent } from './paperwork.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { StayRequestComponent } from './stay-request/stay-request.component';
import { StayRequestForm } from './stay-request-form/stay-request-form.component';
import { ApplianceComponent } from './appliance/appliance.component';
import { ApplianceForm } from './appliance-form/appliance-form.component';
import { InventoryStorageComponent } from './inventory-storage/inventory-storage.component';
import { InventoryStorageDetailsComponent } from './inventory-storage/details/inventory-storage-details.component';
import { DetailsComponent } from './stay-request/details/details.component';

const routes: Routes = [
  {
    path: 'stay-during-break',
    component: StayRequestComponent,
  },
  {
    path: 'appliances',
    component: ApplianceComponent,
  },
  {
    path: 'inventory-storage',
    component: InventoryStorageComponent,
  },
];

@NgModule({
  declarations: [
    PaperworkComponent,
    StayRequestComponent,
    StayRequestForm,
    ApplianceComponent,
    ApplianceForm,
    InventoryStorageComponent,
    InventoryStorageDetailsComponent,
    DetailsComponent,
  ],
  imports: [SharedModule, RouterModule.forChild(routes)],
})
export class PaperworkModule {}
