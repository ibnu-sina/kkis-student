import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Result } from 'src/app/shared/services/rest-interceptor.service';

@Injectable({
  providedIn: 'root',
})
export class PaperworkService {
  constructor(private http: HttpClient) {}

  //stay during break management
  async getStayRequest(): Promise<StayRequest[]> {
    try {
      const result: Result = await this.http
        .get(`/stay-requests/me`)
        .toPromise();
      return result.data;
    } catch (error) {}
  }

  async addStayRequest(data: StayRequestDTO) {
    try {
      await this.http.post(`/stay-requests`, data).toPromise();
    } catch (error) {}
  }

  async editStayRequest(data: StayRequestDTO, id: number) {
    try {
      await this.http.put(`/stay-requests/${id}`, data).toPromise();
    } catch (error) {}
  }

  async deleteStayRequest(id: number) {
    try {
      await this.http.delete(`/stay-requests/${id}`).toPromise();
    } catch (error) {}
  }

  async checkin(id: number) {
    try {
      await this.http
        .put(`/stay-requests/${id}/checkin/student`, '')
        .toPromise();
    } catch (error) {}
  }

  async checkout(id: number) {
    try {
      await this.http
        .put(`/stay-requests/${id}/checkout/student`, '')
        .toPromise();
    } catch (error) {}
  }

  //appliance management
  async getAppliances(): Promise<Appliance[]> {
    try {
      let result: Result = await this.http.get(`/appliances`).toPromise();
      return result.data;
    } catch (error) {}
  }

  async getMyAppliances(): Promise<MyAppliance[]> {
    try {
      let result: Result = await this.http.get(`/appliances/me`).toPromise();
      return result.data;
    } catch (error) {}
  }

  async registerAppliance(data: { appliances: number[] }) {
    try {
      await this.http.post(`/appliances/register`, data).toPromise();
    } catch (error) {}
  }

  //inventory-storage
  async getInventoryRequest(): Promise<InventoryRequest[]> {
    try {
      let result: Result = await this.http
        .get(`/inventory-storages/me`)
        .toPromise();
      return result.data;
    } catch (error) {}
  }

  async addInventoryRequest(): Promise<void> {
    try {
      await this.http.post(`/inventory-storages`, '').toPromise();
    } catch (error) {}
  }

  async deleteInventoryRequest(request_id: number): Promise<void> {
    try {
      await this.http.delete(`/inventory-storages/${request_id}`).toPromise();
    } catch (error) {}
  }
}

export interface StayRequest {
  id?: number;
  student_id?: number;
  start_date?: Date;
  end_date?: Date;
  total_days?: number;
  reason?: string;
  payment_method?: number;
  status?: string;
  price_per_day?: number;
  respond_admin_id?: number;
  paid_admin_id?: number;
  respond_date?: Date;
  paid_date?: Date;
  checkin_date?: Date;
  checkout_Date?: Date;
}

export interface StayRequestDTO {
  start_date?: Date;
  end_date?: Date;
  reason?: string;
  payment_method?: number;
}

export interface Appliance {
  id?: number;
  name?: string;
  price?: number;
  selected?: boolean;
}

export interface MyAppliance {
  id?: number;
  name?: string;
  price?: number;
  pivot?: {
    appliance_id?: number;
    student_id?: number;
    serial_no?: number;
  };
}

export interface InventoryRequest {
  id?: number;
  student?: {
    id?: number;
    name?: string;
  };
  status?: string;
  request_date?: Date;
  request_resolve_date?: Date;
  request_box_no?: number;
  request_description?: string;
  request_admin?: {
    id?: number;
    name?: string;
  };
  request_admin_id?: number;
  request_storekeeper?: {
    id?: number;
    name?: string;
  };
  request_storekeeper_id?: number;
  claim_date?: Date;
  claim_box_no?: number;
  claim_description?: string;
  claim_admin?: {
    id?: number;
    name?: string;
  };
  claim_admin_id?: number;
  claim_claimer_name?: string;
}
