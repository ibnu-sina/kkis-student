import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { UtilService } from 'src/app/shared/services/util.service';
import { Appliance, PaperworkService } from '../paperwork.service';

@Component({
  selector: 'app-appliance-form',
  templateUrl: './appliance-form.component.html',
  styleUrls: ['./appliance-form.component.scss'],
})
export class ApplianceForm implements OnInit {
  @Input() registeredApp: number[] = [1, 11];
  form: FormGroup;
  appliances: Appliance[] = [];
  selectedApp: number[] = [];
  loading: boolean = true;
  totalPrice: number = 0;

  constructor(
    protected ref: NbDialogRef<ApplianceForm>,
    private paperworkService: PaperworkService,
    public util: UtilService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      agreement: [null, Validators.requiredTrue],
      oneSelected: [null, Validators.requiredTrue],
    });
  }

  async ngOnInit() {
    let appliances: Appliance[] = await this.paperworkService.getAppliances();
    appliances.forEach((value) => {
      if (!this.registeredApp.includes(value.id)) this.appliances.push(value);
    });

    this.loading = false;
  }

  onSelect(app: Appliance) {
    app.selected = !app.selected;

    app.selected
      ? (this.totalPrice += app.price)
      : (this.totalPrice -= app.price);

    this.form.patchValue({
      oneSelected: this.appliances.some((val) => val.selected),
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.appliances.forEach((value) => {
        if (value.selected) this.selectedApp.push(value.id);
      });
      this.ref.close(this.selectedApp);
    }
  }

  close() {
    this.ref.close();
  }
}
