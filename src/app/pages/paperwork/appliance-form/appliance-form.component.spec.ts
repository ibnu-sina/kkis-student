import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplianceForm } from './appliance-form.component';

describe('ApplianceForm', () => {
  let component: ApplianceForm;
  let fixture: ComponentFixture<ApplianceForm>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplianceForm],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplianceForm);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
