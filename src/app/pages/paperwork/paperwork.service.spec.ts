import { TestBed } from '@angular/core/testing';

import {
  Appliance,
  InventoryRequest,
  MyAppliance,
  PaperworkService,
  StayRequest,
  StayRequestDTO,
} from './paperwork.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PaperworkService', () => {
  let service: PaperworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PaperworkService],
    });
    service = TestBed.inject(PaperworkService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class PaperworkServiceMock {
  async getStayRequest(): Promise<StayRequest[]> {
    return undefined;
  }
  async addStayRequest(data: StayRequestDTO): Promise<void> {}
  async editStayRequest(data: StayRequestDTO, id: number): Promise<void> {}
  async deleteStayRequest(id: number) {}
  async checkin(id: number) {}
  async checkout(id: number) {}

  async getAppliances(): Promise<Appliance[]> {
    return undefined;
  }
  async getMyAppliances(): Promise<MyAppliance[]> {
    return undefined;
  }
  async registerAppliance(data: { appliances: number[] }) {}

  async getInventoryRequest(): Promise<InventoryRequest[]> {
    return undefined;
  }
  async addInventoryRequest(): Promise<void> {}
  async deleteInventoryRequest(request_id: number): Promise<void> {}
}
