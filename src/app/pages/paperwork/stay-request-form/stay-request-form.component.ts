import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { dateOnly, FormService } from 'src/app/shared/services/form.service';
import { StayRequest, StayRequestDTO } from '../paperwork.service';
import * as moment from 'moment';
@Component({
  selector: 'app-stay-request-form',
  templateUrl: './stay-request-form.component.html',
  styleUrls: ['./stay-request-form.component.scss'],
})
export class StayRequestForm implements OnInit {
  @Input() request: StayRequest;
  form: FormGroup;
  min_start_date: Date;
  max_start_date: Date;
  min_end_date: Date;
  tomorrow: Date;

  constructor(
    private formService: FormService,
    private formBuilder: FormBuilder,
    protected ref: NbDialogRef<StayRequestForm>
  ) {
    this.createForm();
  }

  async ngOnInit() {
    this.min_start_date = new Date();
    this.min_end_date = new Date();
    this.tomorrow = new Date();
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);

    if (this.request) {
      this.patchForm();
    }
    this.listenChanges();
  }

  createForm() {
    this.form = this.formBuilder.group({
      start_date: [null, dateOnly],
      end_date: [null, dateOnly],
      reason: [null, Validators.required],
      payment_method: [null, Validators.required],
    });
  }

  patchForm() {
    let start_date = new Date(this.request.start_date);
    let end_date = new Date(this.request.end_date);
    let { reason, payment_method } = this.request;
    this.form.setValue({ reason, payment_method, start_date, end_date });
  }

  listenChanges() {
    this.form.get('start_date').valueChanges.subscribe((v) => {
      if (moment(v).isBefore(new Date())) {
        this.min_end_date = new Date();
      } else {
        this.min_end_date = v;
      }
    });
    this.form.get('end_date').valueChanges.subscribe((v) => {
      if (moment(v).isBefore(new Date())) {
        this.max_start_date.setDate(new Date().getDate() + 1);
      } else {
        this.max_start_date = v;
      }
    });
  }

  formatDate(date: Date): string {
    return moment(date).format('ll');
  }

  dateIsSame(date1: Date, date2: Date) {
    return moment(date1).isSame(date2, 'day');
  }

  onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      let { start_date, end_date, ...data }: StayRequestDTO = this.form.value;
      start_date = this.formService.formatDate(start_date);
      end_date = this.formService.formatDate(end_date);

      this.ref.close({ start_date, end_date, ...data });
    }
  }

  close() {
    this.ref.close();
  }
}
