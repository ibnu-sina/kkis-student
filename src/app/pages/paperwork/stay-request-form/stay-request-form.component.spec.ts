import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StayRequestForm } from './stay-request-form.component';

describe('StayRequestForm', () => {
  let component: StayRequestForm;
  let fixture: ComponentFixture<StayRequestForm>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StayRequestForm],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StayRequestForm);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
