import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import {
  ModalComponent,
  ModalData,
} from 'src/app/shared/components/modal/modal.component';
import { InventoryRequest, PaperworkService } from '../paperwork.service';

@Component({
  selector: 'app-inventory-storage',
  templateUrl: './inventory-storage.component.html',
  styleUrls: ['./inventory-storage.component.scss'],
})
export class InventoryStorageComponent implements OnInit {
  loading: boolean;
  requests: InventoryRequest[] = [];
  approveRequests: InventoryRequest[] = [];
  pendingRequests: InventoryRequest[] = [];

  constructor(
    private paperworkService: PaperworkService,
    private dialogService: NbDialogService,
    private toastService: NbToastrService
  ) {}

  async ngOnInit() {
    this.loading = true;
    await this.getRequests();
    this.loading = false;
  }

  async getRequests() {
    this.requests = await this.paperworkService.getInventoryRequest();

    this.pendingRequests = [];
    this.approveRequests = [];

    this.requests.forEach((request) => {
      if (request.status === 'pending') {
        this.pendingRequests.push(request);
      } else {
        this.approveRequests.push(request);
      }
    });
  }

  async addRequest() {
    let pendingRequest = this.requests.some(
      (request) => request.status === 'pending'
    );

    if (pendingRequest) {
      let modalData: ModalData = {
        title: 'Alert',
        description:
          'A pending request has not been resolved. Are you sure you want to make a new request?',
        cancelButtonText: 'Cancel',
        okButtonText: 'Yes',
      };
      this.dialogService
        .open(ModalComponent, { context: { modalData } })
        .onClose.subscribe(async (create: boolean) => {
          if (create) {
            await this.createRequest();
          }
        });
    } else {
      await this.createRequest();
    }
  }

  private async createRequest() {
    await this.paperworkService.addInventoryRequest();
    this.toastService.show(
      'Your request is waiting to be reviewed by the college management',
      'Request has been made successfully!',
      { duration: 3000 }
    );
    await this.getRequests();
  }

  async deleteRequest(id: number) {
    this.dialogService
      .open(DeleteModalComponent)
      .onClose.subscribe(async (deleteRequest) => {
        if (deleteRequest) {
          await this.paperworkService.deleteInventoryRequest(id);
          await this.getRequests();
        }
      });
    await this.getRequests();
  }
}
