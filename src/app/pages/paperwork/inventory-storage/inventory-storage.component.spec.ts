import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryStorageComponent } from './inventory-storage.component';

describe('InventoryStorageComponent', () => {
  let component: InventoryStorageComponent;
  let fixture: ComponentFixture<InventoryStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InventoryStorageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
