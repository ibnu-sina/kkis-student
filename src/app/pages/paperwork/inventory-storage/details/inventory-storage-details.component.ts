import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import { InventoryRequest, PaperworkService } from '../../paperwork.service';
import * as moment from 'moment';

@Component({
  selector: 'app-inventory-storage-details',
  templateUrl: './inventory-storage-details.component.html',
  styleUrls: ['./inventory-storage-details.component.scss'],
})
export class InventoryStorageDetailsComponent implements OnInit {
  @Input() request: InventoryRequest;
  @Output() requestDeleted = new EventEmitter<void>();

  constructor(
    private paperworkService: PaperworkService,
    private dialogService: NbDialogService
  ) {}

  ngOnInit() {}

  async delete() {
    this.dialogService
      .open(DeleteModalComponent)
      .onClose.subscribe(async (deleteRequest) => {
        if (deleteRequest) {
          await this.paperworkService.deleteInventoryRequest(this.request.id);
          this.requestDeleted.emit();
        }
      });
  }

  formatDate(date: Date): string {
    return moment(date).format('Do MMMM YYYY');
  }
}
