import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paperwork',
  templateUrl: './paperwork.component.html',
  styleUrls: ['./paperwork.component.scss'],
})
export class PaperworkComponent implements OnInit {
  forms = [
    {
      label: 'Appliance Registration',
      onClick: () => this.sth(),
    },
    // {
    //   label: 'Van booking',
    //   onClick: () => this.goto('van-booking'),
    // },
    // {
    //   label: 'Food order',
    //   onClick: () => this.goto('food-order'),
    // },
    {
      label: 'Stay during Semester Break',
      onClick: () => this.goto('stay-during-break'),
    },
    {
      label: '"Jaminan simpan barang"',
      onClick: () => this.sth(),
    },
    // {
    //   label: 'Report issues',
    //   onClick: () => this.dosth()
    // },
    // {
    //   label: 'Equipment booking',
    //   onClick: () => this.goto('equipment-booking'),
    // },
  ];
  constructor(private router: Router) {}

  ngOnInit() {}

  goto(path: string) {
    this.router.navigateByUrl(`paperwork/${path}`);
  }

  sth() {}
}
