import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StayRequestComponent } from './stay-request.component';

describe('StayRequestComponent', () => {
  let component: StayRequestComponent;
  let fixture: ComponentFixture<StayRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StayRequestComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StayRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
