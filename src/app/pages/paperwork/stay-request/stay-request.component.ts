import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import {
  PaperworkService,
  StayRequest,
  StayRequestDTO,
} from '../paperwork.service';
import { StayRequestForm } from '../stay-request-form/stay-request-form.component';
import * as moment from 'moment';
import { DecimalPipe } from '@angular/common';
import {
  ModalComponent,
  ModalData,
} from 'src/app/shared/components/modal/modal.component';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';

@Component({
  selector: 'app-stay-request',
  templateUrl: './stay-request.component.html',
  styleUrls: ['./stay-request.component.scss'],
})
export class StayRequestComponent implements OnInit {
  loading: boolean;
  uploading: boolean;
  requests: StayRequest[] = [];
  approveRequests: StayRequest[] = [];
  pendingRequests: StayRequest[] = [];

  constructor(
    private paperworkService: PaperworkService,
    private dialogService: NbDialogService,
    private toastService: NbToastrService
  ) {}

  async ngOnInit() {
    this.loading = true;
    await this.getRequests();
    this.loading = false;
  }

  async getRequests() {
    this.requests = await this.paperworkService.getStayRequest();

    this.pendingRequests = [];
    this.approveRequests = [];

    this.requests.forEach((r) => {
      if (r.status === 'pending') {
        this.pendingRequests.push(r);
      } else {
        this.approveRequests.push(r);
      }
    });
  }

  addRequest() {
    this.dialogService
      .open(StayRequestForm)
      .onClose.subscribe(async (data: StayRequestDTO) => {
        if (data) {
          this.uploading = true;
          try {
            await this.paperworkService.addStayRequest(data);
            this.toastService.show(
              'Your application has been submitted and waiting to be reviewed. You can still edit your application while waiting for it to be approved.',
              'Application sent successfully',
              { duration: 3000 }
            );
            await this.getRequests();
          } catch (error) {}
          this.uploading = false;
        }
      });
  }

  editRequest(request: StayRequest) {
    this.dialogService
      .open(StayRequestForm, { context: { request } })
      .onClose.subscribe(async (data: StayRequestDTO) => {
        if (data) {
          this.uploading = true;
          try {
            await this.paperworkService.editStayRequest(data, request.id);
            this.toastService.show(
              'Your application is updated and waiting to be reviewed',
              'Application updated successfully'
            );
            await this.getRequests();
          } catch (error) {}
          this.uploading = false;
        }
      });
  }

  deleteRequest(request: StayRequest) {
    this.dialogService
      .open(DeleteModalComponent)
      .onClose.subscribe(async (deleteApplication: boolean) => {
        if (deleteApplication) {
          this.uploading = true;
          try {
            await this.paperworkService.deleteStayRequest(request.id);
            this.toastService.show(
              'Your stay during semester break application has been removed',
              'Application removed successfully'
            );
            await this.getRequests();
          } catch (error) {}
          this.uploading = false;
        }
      });
  }

  formatDate(date: Date): string {
    if (date) {
      return moment(date).format('Do MMM  YYYY');
    }
  }

  formatPrice(amount: number): string {
    return 'RM' + new DecimalPipe('en-US').transform(amount, '1.2-2');
  }

  async checkin(request: StayRequest) {
    let today = new Date();
    let { start_date } = request;

    if (moment(today).isSameOrAfter(start_date, 'day')) {
      this.uploading = true;
      try {
        await this.paperworkService.checkin(request.id);
        await this.getRequests();
      } catch (error) {}
      this.uploading = false;
    } else {
      let modalData: ModalData = {
        title: 'Alert',
        description: `You'll be allowed to checkin on ${moment(
          request.start_date
        ).format('Do MMM  YYYY (dddd)')}`,
      };
      this.dialogService.open(ModalComponent, { context: { modalData } });
    }
  }

  async checkout(request: StayRequest) {
    let today = new Date();
    let { end_date } = request;

    if (request.checkin_date) {
      this.uploading = true;
      try {
        await this.paperworkService.checkout(request.id);
        await this.getRequests();
      } catch (error) {}
      this.uploading = false;
    } else {
      let modalData: ModalData = {
        title: 'Alert',
        description: `You'll be allowed to checkout on ${moment(
          request.end_date
        ).format('Do MMM  YYYY (dddd)')} once you have checked-in`,
      };
      this.dialogService.open(ModalComponent, { context: { modalData } });
    }
  }
}
