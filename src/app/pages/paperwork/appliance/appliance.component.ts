import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { UtilService } from 'src/app/shared/services/util.service';
import { ApplianceForm } from '../appliance-form/appliance-form.component';
import { MyAppliance, PaperworkService } from '../paperwork.service';

@Component({
  selector: 'app-appliance',
  templateUrl: './appliance.component.html',
  styleUrls: ['./appliance.component.scss'],
})
export class ApplianceComponent implements OnInit {
  loading: boolean = true;
  appliances: MyAppliance[] = [];
  registeredApp: number[] = [];
  constructor(
    private dialogService: NbDialogService,
    private paperworkService: PaperworkService,
    public util: UtilService,
    private toastService: NbToastrService
  ) {}

  async ngOnInit() {
    await this.getAppliances();
    this.loading = false;
  }

  async getAppliances() {
    this.appliances = await this.paperworkService.getMyAppliances();
    this.appliances.forEach((app) => {
      this.registeredApp.push(app.pivot.appliance_id);
    });
  }

  registerAppliance() {
    this.dialogService
      .open(ApplianceForm, {
        context: { registeredApp: this.registeredApp },
      })
      .onClose.subscribe(async (appliances: number[]) => {
        if (appliances)
          if (appliances.length > 0)
            try {
              await this.paperworkService.registerAppliance({ appliances });

              this.toastService.show(
                'Stop by the management office to collect your sticker',
                'Registration succesful!',
                { duration: 5000 }
              );

              await this.getAppliances();
            } catch (error) {}
      });
  }
}
