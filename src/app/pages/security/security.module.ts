import { NgModule } from '@angular/core';
import { SecurityComponent } from './security.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [
  {
    path: '',
    component: SecurityComponent
  }
];

@NgModule({
  declarations: [SecurityComponent, ChangePasswordComponent],
  imports: [SharedModule, RouterModule.forChild(routes)]
})
export class SecurityModule {}
