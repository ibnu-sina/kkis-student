import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { NbToastrServiceMock } from 'src/app/mocks/NbToastrServiceMock';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AuthServiceMock } from 'src/app/shared/services/auth.service.spec';
import { FormService } from 'src/app/shared/services/form.service';
import { FormServiceMock } from 'src/app/shared/services/form.service.spec';

import { ChangePasswordComponent } from './change-password.component';

describe('ChangePasswordComponent', () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChangePasswordComponent],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: FormService, useClass: FormServiceMock },
        { provide: NbToastrService, useClass: NbToastrServiceMock },
      ],
      imports: [ReactiveFormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
