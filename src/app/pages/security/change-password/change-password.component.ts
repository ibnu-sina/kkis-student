import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { AuthService } from 'src/app/shared/services/auth.service';
import { FormService } from 'src/app/shared/services/form.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss', '../security.component.scss'],
})
export class ChangePasswordComponent {
  form: FormGroup;
  showOldpass: boolean;
  showPassword: boolean;
  showRepeat: boolean;
  oldpassError: string;
  loading: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private formService: FormService,
    private authService: AuthService,
    private toastService: NbToastrService
  ) {
    this.createForm();
    this.listenChanges();
  }

  listenChanges() {
    this.form.get('old_password').valueChanges.subscribe((value) => {
      this.oldpassError = undefined;
    });
  }

  createForm() {
    this.form = this.formBuilder.group(
      {
        old_password: [null, Validators.required],
        password: [null, Validators.required],
        repeat: [null, Validators.required],
      },
      {
        validators: [this.formService.equalPassword('password', 'repeat')],
      }
    );
  }

  async onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      let { repeat, ...data } = this.form.value;
      this.loading = true;
      try {
        await this.authService.changePassword(data);
        this.toastService.show(
          'Your password has been changed successfully. Use your new password to log in.',
          'Password Updated!',
          {
            icon: 'checkmark-circle-outline',
          }
        );
        this.showOldpass = this.showPassword = this.showRepeat = false;
        this.form.reset();
      } catch (error) {
        switch (error.error.message) {
          case 'Invalid password':
            this.oldpassError = 'Your current password is incorrect';
        }
      }
      this.loading = false;
    }
  }
}
