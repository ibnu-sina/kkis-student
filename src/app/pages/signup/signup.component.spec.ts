import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NbDialogService } from '@nebular/theme';
import { NbDialogServiceMock } from 'src/app/mocks/NbDialogServiceMock';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AuthServiceMock } from 'src/app/shared/services/auth.service.spec';
import { FormService } from 'src/app/shared/services/form.service';
import { FormServiceMock } from 'src/app/shared/services/form.service.spec';

import { SignupPage } from './signup.component';

describe('SignupPage', () => {
  let component: SignupPage;
  let fixture: ComponentFixture<SignupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignupPage],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: FormService, useClass: FormServiceMock },
        { provide: NbDialogService, useClass: NbDialogServiceMock },
      ],
      imports: [ReactiveFormsModule, RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
