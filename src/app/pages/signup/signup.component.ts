import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import {
  AuthService,
  SignupTokenResponse,
} from 'src/app/shared/services/auth.service';
import { FormService } from 'src/app/shared/services/form.service';
import {
  ModalComponent,
  ModalData,
} from 'src/app/shared/components/modal/modal.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupPage implements OnInit {
  form: FormGroup;
  loading: boolean;
  uploading: boolean;
  tokenResponse: SignupTokenResponse;
  errorMessage: string;
  successMessage: string;
  showPassword: boolean;
  showRepeat: boolean;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private formService: FormService,
    private dialogService: NbDialogService
  ) {
    this.createForm();
  }

  async ngOnInit() {
    this.loading = true;
    await this.checkStatus();
    this.loading = false;
  }

  createForm() {
    this.form = this.formBuilder.group(
      {
        password: [null, Validators.required],
        confirm_password: [null, Validators.required],
      },
      {
        validators: [
          this.formService.equalPassword('password', 'confirm_password'),
        ],
      }
    );
  }

  async checkStatus() {
    let token = this.route.snapshot.paramMap.get('token');
    try {
      this.tokenResponse = await this.authService.checkSignupToken(token);
    } catch (error) {
      let modalData: ModalData = {
        title: 'Alert',
        description: error.error.message,
      };

      switch (error.error.message) {
        case 'Not in invite list':
          modalData.description = `You're not allowed to create an account for this system`;
          break;
        case 'Account already activated':
          modalData = {
            description: `You have already created an account for this system.`,
            cancelButtonText: 'Login now!',
            onClose: () => this.router.navigate(['login']),
          };
          break;
      }

      this.dialogService.open(ModalComponent, { context: { modalData } });
      this.form.disable();
    }
  }

  async onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      let { room_id, token } = this.tokenResponse;
      let data = { ...this.form.value, token, room_id };
      this.uploading = true;

      try {
        await this.authService.signup(data);

        let modalData: ModalData = {
          title: 'Account setup is successful!',
          description: 'You may now login using your email and password',
          cancelButtonText: 'Login now!',
          onClose: () => this.router.navigate(['login']),
        };
        this.dialogService.open(ModalComponent, {
          context: { modalData },
          closeOnBackdropClick: false,
        });
      } catch (error) {
        this.errorMessage = error.error.message;
      }
      this.uploading = false;
    }
  }
}
