import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NbToastrService } from '@nebular/theme';
import { MockModule } from 'src/app/mocks/mock.module';
import { NbToastrServiceMock } from 'src/app/mocks/NbToastrServiceMock';
import { FormService } from 'src/app/shared/services/form.service';
import { FormServiceMock } from 'src/app/shared/services/form.service.spec';
import { UserService } from 'src/app/shared/services/user.service';
import { UserServiceMock } from 'src/app/shared/services/user.service.spec';

import { ProfileForm } from './profile-form.component';

describe('ProfileForm', () => {
  let component: ProfileForm;
  let fixture: ComponentFixture<ProfileForm>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MockModule],
      providers: [
        { provide: UserService, useClass: UserServiceMock },
        { provide: FormService, useClass: FormServiceMock },
        { provide: NbToastrService, useClass: NbToastrServiceMock },
      ],
      declarations: [ProfileForm],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileForm);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("empty form is invalid when 'Save' button is clicked  ", async () => {
    let spy = spyOn(component, 'onSubmit');

    let button = fixture.debugElement.nativeElement.querySelector(
      'button#submit'
    );
    button.click();

    await fixture.whenStable();

    expect(spy).toHaveBeenCalled();
    expect(component.form.valid).toBeFalsy();
  });

  it('form is valid when validators are fulfilled ', async () => {
    let spy = spyOn(component, 'onSubmit');
    component.form.patchValue({
      ic_no: '981019105808',
      phone_no: '0189810772',
      gender: 'female',
      race: 'Malay',
      religion: 'Islam',
      birth_date: new Date('1998-10-19'),
      birth_place: 'Klang',
    });

    let button = fixture.debugElement.nativeElement.querySelector(
      'button#submit'
    );
    button.click();

    await fixture.whenStable();

    expect(spy).toHaveBeenCalled();
    expect(component.form.valid).toBeTruthy();
  });

  it("reset changes when 'Save' button is clicked", async () => {
    let spy = spyOn(component, 'patchForm');

    let button = fixture.debugElement.nativeElement.querySelector(
      'button#cancel'
    );
    button.click();

    await fixture.whenStable();

    expect(spy).toHaveBeenCalled();
  });

  it('check controls disability', () => {
    expect(component.form.get('name').disabled).toBeTruthy();
    expect(component.form.get('ic_no').disabled).toBeFalsy();
    expect(component.form.get('email').disabled).toBeTruthy();
    expect(component.form.get('phone_no').disabled).toBeFalsy();
    expect(component.form.get('gender').disabled).toBeFalsy();
    expect(component.form.get('race').disabled).toBeFalsy();
    expect(component.form.get('religion').disabled).toBeFalsy();
    expect(component.form.get('birth_date').disabled).toBeFalsy();
    expect(component.form.get('birth_place').disabled).toBeFalsy();
    expect(component.form.get('matric_no').disabled).toBeTruthy();
    expect(component.form.get('room').disabled).toBeTruthy();
    expect(component.form.get('course').disabled).toBeTruthy();
  });

  it('check ic_no validity', () => {
    component.form.patchValue({ ic_no: null });
    expect(component.form.get('ic_no').valid).toBeFalsy();

    component.form.patchValue({ ic_no: 'hello' });
    expect(component.form.get('ic_no').valid).toBeFalsy();

    component.form.patchValue({ ic_no: '99101910580' });
    expect(component.form.get('ic_no').valid).toBeFalsy();

    component.form.patchValue({ ic_no: '9910191058099' });
    expect(component.form.get('ic_no').valid).toBeFalsy();

    component.form.patchValue({ ic_no: '991019105808' });
    expect(component.form.get('ic_no').valid).toBeTruthy();
  });

  it('check gender validity', () => {
    component.form.patchValue({ gender: null });
    expect(component.form.get('gender').valid).toBeFalsy();

    component.form.patchValue({ gender: 'male' });
    expect(component.form.get('gender').valid).toBeTruthy();

    component.form.patchValue({ gender: 'female' });
    expect(component.form.get('gender').valid).toBeTruthy();
  });

  it('check phone_no validity', () => {
    component.form.patchValue({ phone_no: null });
    expect(component.form.get('phone_no').valid).toBeFalsy();

    component.form.patchValue({ phone_no: '012345678' });
    expect(component.form.get('phone_no').valid).toBeFalsy();

    component.form.patchValue({ phone_no: '011234567890' });
    expect(component.form.get('phone_no').valid).toBeFalsy();

    component.form.patchValue({ phone_no: 'a_phone_no' });
    expect(component.form.get('phone_no').valid).toBeFalsy();

    component.form.patchValue({ phone_no: '0123456789' });
    expect(component.form.get('phone_no').valid).toBeTruthy();

    component.form.patchValue({ phone_no: '01123456789' });
    expect(component.form.get('phone_no').valid).toBeTruthy();
  });

  it('check birth_date validity', () => {
    let today = new Date();
    let tomorrow = new Date().getDate() + 1;
    let birth_date = new Date('1998-10-19');

    component.form.patchValue({ birth_date: null });
    expect(component.form.get('birth_date').valid).toBeFalsy();

    component.form.patchValue({ birth_date: tomorrow });
    expect(component.form.get('birth_date').valid).toBeFalsy();

    component.form.patchValue({ birth_date: today });
    expect(component.form.get('birth_date').valid).toBeTruthy();

    component.form.patchValue({ birth_date });
    expect(component.form.get('birth_date').valid).toBeTruthy();
  });
});
