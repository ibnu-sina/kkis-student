import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { User, UserService } from 'src/app/shared/services/user.service';
import {
  dateOnly,
  FormService,
  numberOnly,
} from 'src/app/shared/services/form.service';
@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss'],
})
export class ProfileForm implements OnInit, OnChanges {
  @Input() user: User;
  form: FormGroup;
  loading: boolean;
  today: Date;
  religions: string[] = [
    'Islam',
    'Buddhism',
    'Christianity',
    'Hinduism',
    'Chinese religions',
  ];
  races: string[] = ['Malay', 'Bumiputra', 'Chinese', 'Indians', 'Others'];

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toastService: NbToastrService,
    private formService: FormService
  ) {
    this.createForm();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.user.currentValue) {
      this.patchForm();
    }
  }

  ngOnInit() {
    this.today = new Date();
  }

  createForm() {
    let value = null;
    let disabled: boolean = true;

    this.form = this.formBuilder.group({
      name: [{ value, disabled }],
      ic_no: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(12),
          Validators.maxLength(12),
          numberOnly,
        ]),
      ],
      email: [{ value, disabled }],
      phone_no: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(11),
          numberOnly,
        ]),
      ],
      gender: [null, Validators.required],
      race: [null, Validators.required],
      religion: [null, Validators.required],
      birth_date: [null, dateOnly],
      birth_place: [null, Validators.required],
      matric_no: [{ value, disabled }],
      room: [{ value, disabled }],
      course: [{ value, disabled }],
    });
  }

  patchForm() {
    this.form.patchValue({
      ...this.user,
      course: this.user.course.name,
      room: this.user.room.name,
    });

    if (this.user.birth_date) {
      let birth_date = new Date(this.user.birth_date);
      this.form.patchValue({ birth_date });
    }
  }

  async onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      let { birth_date, ...data }: User = this.form.value;
      birth_date = this.formService.formatDate(birth_date);

      this.loading = true;

      try {
        await this.userService.editUser({ birth_date, ...data });
        this.toastService.show(
          'Your information has been saved successfully.',
          'Data saved successfully'
        );
      } catch (error) {}

      this.loading = false;
    }
  }
}
