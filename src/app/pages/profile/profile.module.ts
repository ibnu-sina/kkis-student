import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from 'src/app/pages/profile/profile.component';
import { ProfileForm } from './profile-form/profile-form.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  },
];

@NgModule({
  declarations: [ProfileComponent, ProfileForm],
  imports: [SharedModule, RouterModule.forChild(routes)],
})
export class ProfileModule {}
