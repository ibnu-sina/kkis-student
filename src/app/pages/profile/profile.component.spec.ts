import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MockModule } from 'src/app/mocks/mock.module';
import { UserService } from 'src/app/shared/services/user.service';
import { UserServiceMock } from 'src/app/shared/services/user.service.spec';
import { ProfileComponent } from './profile.component';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MockModule],
      declarations: [ProfileComponent],
      providers: [{ provide: UserService, useClass: UserServiceMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init properly', async () => {
    spyOn(component, 'getUser').and.callThrough();
    const userService = TestBed.inject(UserService);
    spyOn(userService, 'getUser').and.returnValue(
      Promise.resolve({
        name: 'Nazurah',
      })
    );

    expect(component.getUser).not.toHaveBeenCalled();

    component.ngOnInit();

    expect(component.getUser).toHaveBeenCalled();
    expect(userService.getUser).toHaveBeenCalled();

    await fixture.whenStable();
    expect(component.user).toBeDefined();
  });
});
