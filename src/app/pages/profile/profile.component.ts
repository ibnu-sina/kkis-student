import { Component, OnInit } from '@angular/core';
import { User, UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  user: User;
  loading: boolean;

  constructor(private userService: UserService) {}

  async ngOnInit() {
    await this.getUser();
  }

  async getUser() {
    this.loading = true;
    this.user = await this.userService.getUser();
    this.loading = false;
  }
}
