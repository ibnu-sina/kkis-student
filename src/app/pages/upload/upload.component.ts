import { Component, OnInit } from '@angular/core';
import { Upload, UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
})
export class UploadComponent implements OnInit {
  loading: boolean;
  signature: Upload;
  meritUploads: Upload[] = [];
  constructor(private util: UtilService) {}

  async ngOnInit() {
    this.loading = true;

    let uploads: Upload[] = await this.util.getUploads('signature');
    this.signature = uploads[0];

    this.meritUploads = await this.util.getUploads('merit');

    this.loading = false;
  }
}
