import { Component, Input, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { DrawSignatureModal } from 'src/app/shared/components/draw-signature/draw-signature.component';
import { Upload, UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-signature',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.scss'],
})
export class SignatureComponent implements OnInit {
  @Input() signature: Upload;
  signatureImage: string;
  loading: boolean;

  constructor(
    private util: UtilService,
    private dialogService: NbDialogService
  ) {}

  ngOnInit() {}

  async getSignature() {
    let signatures: Upload[] = await this.util.getUploads('signature');
    this.signature = signatures[0];
  }

  add() {
    this.dialogService
      .open(DrawSignatureModal)
      .onClose.subscribe(async (form: FormData) => {
        if (form) {
          this.loading = true;
          try {
            await this.util.uploadFile('signature', form);
            await this.getSignature();
            this.loading = false;
          } catch (error) {}
        }
      });
  }

  edit() {
    this.dialogService
      .open(DrawSignatureModal)
      .onClose.subscribe(async (form: FormData) => {
        if (form) {
          this.loading = true;
          try {
            await this.util.deleteUpload(this.signature.id);
            await this.util.uploadFile('signature', form);
            await this.getSignature();
            this.loading = false;
          } catch (error) {}
        }
      });
  }
}
