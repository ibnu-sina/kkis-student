import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NbDialogService } from '@nebular/theme';
import { MockModule } from 'src/app/mocks/mock.module';
import { NbDialogServiceMock } from 'src/app/mocks/NbDialogServiceMock';
import { UtilService } from 'src/app/shared/services/util.service';
import { UtilServiceMock } from 'src/app/shared/services/util.service.spec';

import { SignatureComponent } from './signature.component';

describe('SignatureComponent', () => {
  let component: SignatureComponent;
  let fixture: ComponentFixture<SignatureComponent>;
  const utilService = jasmine.createSpyObj<UtilService>('UtilService', [
    'getUploads',
    'uploadFile',
    'deleteUpload',
  ]);
  const dialogService = jasmine.createSpyObj<NbDialogService>(
    'NbDialogService',
    ['open']
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MockModule],
      declarations: [SignatureComponent],
      providers: [
        { provide: UtilService, useClass: UtilServiceMock },
        { provide: NbDialogService, useClass: NbDialogServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('.empty-data should render when signature is undefined', () => {
    expect(fixture.nativeElement.querySelector('.empty-data')).not.toBeNull();
  });

  it('img should render when signature is defined', async () => {
    component.signature = {
      id: 241,
      type: 'signature',
      path: 'signature/student_11_1610101204199.',
      url:
        'https://avicennasixcollege.s3-ap-southeast-1.amazonaws.com/signature/student_11_1610101204199.',
      student_id: 11,
    };

    fixture.detectChanges();
    await fixture.whenRenderingDone();

    let img: HTMLImageElement = fixture.nativeElement.querySelector(
      'img.signature'
    );
    let button: HTMLButtonElement = fixture.nativeElement.querySelector(
      'button#edit'
    );

    expect(img).not.toBeNull();
    expect(img.src).toBe(component.signature.url);
    expect(button).not.toBeNull();
  });

  it('button#add should be defined and call add() when clicked', () => {
    spyOn(component, 'add');

    let button = fixture.nativeElement.querySelector('button#add');
    button.click();
    expect(component.add).toHaveBeenCalled();
  });

  it('button#edit should be defined and call edit() when clicked', async () => {
    spyOn(component, 'edit');
    component.signature = {
      id: 241,
      type: 'signature',
      path: 'signature/student_11_1610101204199.',
      url:
        'https://avicennasixcollege.s3-ap-southeast-1.amazonaws.com/signature/student_11_1610101204199.',
      student_id: 11,
    };

    fixture.detectChanges();
    await fixture.whenRenderingDone();

    let button = fixture.nativeElement.querySelector('button#edit');

    expect(button).not.toBeNull();
    button.click();
    expect(component.edit).toHaveBeenCalled();
  });
});
