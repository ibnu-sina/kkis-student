import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NbDialogService } from '@nebular/theme';
import { MockModule } from 'src/app/mocks/mock.module';
import { NbDialogServiceMock } from 'src/app/mocks/NbDialogServiceMock';
import { UtilService } from 'src/app/shared/services/util.service';
import { UtilServiceMock } from 'src/app/shared/services/util.service.spec';
import { MeritComponent } from './merit.component';

describe('MeritComponent', () => {
  let component: MeritComponent;
  let fixture: ComponentFixture<MeritComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MockModule],
      declarations: [MeritComponent],
      providers: [
        { provide: UtilService, useClass: UtilServiceMock },
        { provide: NbDialogService, useClass: NbDialogServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeritComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('list should rendered when uploads are defined ', async () => {
    component.uploads = [
      {
        id: 381,
        type: 'merit',
        path: 'merit/student_11_Merit_transcript_1718.pdf',
        url:
          'https://avicennasixcollege.s3-ap-southeast-1.amazonaws.com/merit/student_11_Merit_transcript_1718.pdf',
        student_id: 11,
      },
    ];

    fixture.detectChanges();
    await fixture.whenRenderingDone();

    let lists = fixture.nativeElement.querySelectorAll('.list');

    expect(lists.length).toBe(1);
  });

  it('.empty-data should rendered when uploads are empty', () => {
    expect(fixture.nativeElement.querySelector('.empty-data')).not.toBeNull();
  });

  it('button in .empty-data should call add() when clicked', () => {
    spyOn(component, 'add');
    let button = fixture.nativeElement.querySelector('.empty-data button');

    button.click();

    expect(component.add).toHaveBeenCalled();
  });

  it('button in .list should call delete() when clicked', async () => {
    spyOn(component, 'delete');

    component.uploads = [
      {
        id: 381,
        type: 'merit',
        path: 'merit/student_11_Merit_transcript_1718.pdf',
        url:
          'https://avicennasixcollege.s3-ap-southeast-1.amazonaws.com/merit/student_11_Merit_transcript_1718.pdf',
        student_id: 11,
      },
    ];
    let upload = component.uploads[0];

    fixture.detectChanges();
    await fixture.whenRenderingDone();

    let lists = fixture.nativeElement.querySelectorAll('.list');
    let button = lists[0].querySelector('button');
    let a = lists[0].querySelector('a');

    button.click();

    expect(component.delete).toHaveBeenCalled();
    expect(component.delete).toHaveBeenCalledWith(upload.id);
    expect(a.href).toBe(upload.url);
    expect(a.textContent).toBe('Merit_transcript_1718.pdf');
  });

  it('add button should be defined and call add() when clicked', async () => {
    spyOn(component, 'add');
    component.uploads = [
      {
        id: 381,
        type: 'merit',
        path: 'merit/student_11_Merit_transcript_1718.pdf',
        url:
          'https://avicennasixcollege.s3-ap-southeast-1.amazonaws.com/merit/student_11_Merit_transcript_1718.pdf',
        student_id: 11,
      },
    ];

    fixture.detectChanges();
    await fixture.whenRenderingDone();

    let button = fixture.nativeElement.querySelector('button');

    button.click();

    expect(component.add).toHaveBeenCalled();
  });
});
