import { Component, Input, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { AddAttachmentComponent } from 'src/app/shared/components/add-attachment/add-attachment.component';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import {
  ModalComponent,
  ModalData,
} from 'src/app/shared/components/modal/modal.component';
import { Upload, UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-merit',
  templateUrl: './merit.component.html',
  styleUrls: ['./merit.component.scss'],
})
export class MeritComponent implements OnInit {
  @Input() uploads: Upload[] = [];
  loading: boolean = false;

  constructor(
    private util: UtilService,
    private dialogService: NbDialogService
  ) {}

  ngOnInit() {}

  async getUploads() {
    this.uploads = await this.util.getUploads('merit');
  }

  formatName(upload: Upload): string {
    let path: string = upload.path;
    path = path.replace(`${upload.type}/student_${upload.student_id}_`, '');
    return path;
  }

  add() {
    this.dialogService
      .open(AddAttachmentComponent)
      .onClose.subscribe(async (files: File[]) => {
        if (files) {
          this.loading = true;
          for (let file of files) {
            let form = new FormData();
            form.append('file', file);
            await this.util.uploadFile('merit', form);
          }
          await this.getUploads();

          this.loading = false;
        }
      });
  }

  delete(id: number) {
    this.dialogService
      .open(DeleteModalComponent)
      .onClose.subscribe(async (deleteUpload: boolean) => {
        if (deleteUpload) {
          this.loading = true;
          try {
            await this.util.deleteUpload(id);
            await this.getUploads();
          } catch (error) {
            let modalData: ModalData = {
              title: 'Alert',
              description: error.error.message,
            };

            this.dialogService.open(ModalComponent, { context: { modalData } });
          }
          this.loading = false;
        }
      });
  }
}
