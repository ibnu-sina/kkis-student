import { NgModule } from '@angular/core';
import { UploadComponent } from './upload.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { MeritComponent } from './merit/merit.component';
import { SignatureComponent } from './signature/signature.component';

const routes: Routes = [
  {
    path: '',
    component: UploadComponent,
  },
];

@NgModule({
  declarations: [UploadComponent, MeritComponent, SignatureComponent],
  imports: [SharedModule, RouterModule.forChild(routes)],
})
export class UploadModule {}
