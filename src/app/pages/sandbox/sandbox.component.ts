import { Component, OnInit } from '@angular/core';
import { PaperworkService } from '../paperwork/paperwork.service';
import * as moment from 'moment';
import { DecimalPipe } from '@angular/common';
@Component({
  selector: 'app-sandbox',
  templateUrl: './sandbox.component.html',
  styleUrls: ['./sandbox.component.scss'],
})
export class SandboxComponent implements OnInit {
  constructor(private paperworkService: PaperworkService) {}

  async ngOnInit() {}

  formatID(id: number) {
    return `#IS${new DecimalPipe('en-US').transform(id, '1.2-2')}`;
  }

  formatDate(date: Date) {
    if (date) {
      return moment(date).format('L');
    }
  }
}
