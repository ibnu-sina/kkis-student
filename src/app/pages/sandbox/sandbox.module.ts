import { NgModule } from '@angular/core';
import { SandboxComponent } from './sandbox.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SandboxService } from './sandbox.service';

const routes: Routes = [
  {
    path: '',
    component: SandboxComponent,
  },
];

@NgModule({
  declarations: [SandboxComponent],
  imports: [SharedModule, RouterModule.forChild(routes)],
  providers: [SandboxService],
})
export class SandboxModule {}
