import { TestBed } from '@angular/core/testing';

import { SandboxService } from './sandbox.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SandboxService', () => {
  let service: SandboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SandboxService],
    });
    service = TestBed.inject(SandboxService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class SandboxServiceMock {}
