import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { NbDialogService } from '@nebular/theme';
import {
  ModalData,
  ModalComponent,
} from 'src/app/shared/components/modal/modal.component';
import { User, UserService } from 'src/app/shared/services/user.service';
import { CheckInModal } from './check-in/check-in.component';
import { CheckOutModal } from './check-out/check-out.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  user: User;
  loading: boolean = true;

  constructor(
    private router: Router,
    private dashboardService: DashboardService,
    private dialogService: NbDialogService,
    private userService: UserService
  ) {}

  async ngOnInit() {
    this.user = await this.userService.getUser();
    this.loading = false;
  }

  async onClick(path: string) {
    switch (path) {
      case 'check-in':
        this.checkin();
        break;
      case 'check-out':
        this.checkout();
        break;
      default:
        this.router.navigate([path]);
    }
  }

  checkinOnClick() {
    if (this.user.room_status === 'checked_out') {
      let modalData: ModalData = {
        title: 'Wait a minute',
        description:
          'Is this your first time check-in for this year of academic session?',
        cancelButtonText: 'No',
        okButtonText: 'Yes',
      };

      this.dialogService
        .open(ModalComponent, { context: { modalData } })
        .onClose.subscribe(async (data: boolean) => {
          if (data) {
            this.dialogService
              .open(CheckInModal)
              .onClose.subscribe(async (data: boolean) => {
                if (data) {
                  await this.checkin();
                }
              });
          }

          if (data === false) {
            await this.checkin();
          }
        });
    }
  }

  async checkin() {
    let modalData: ModalData;
    if (this.user.room_status === 'checked_out') {
      try {
        await this.dashboardService.checkin();

        modalData = {
          title: 'Check in request sent',
          description:
            'Your room check in status is waiting to be reviewed. Please wait for a moment.',
          cancelButtonText: 'Okay',
        };
        this.dialogService.open(ModalComponent, { context: { modalData } });
      } catch (error) {
        switch (error.error.messageCode) {
          case 'activity_request_fail':
            modalData = {
              title: 'Alert',
              description:
                'Application for check in is not available for you at the moment',
            };
            this.dialogService.open(ModalComponent, { context: { modalData } });
            break;

          case 'duplicate_activity':
            modalData = {
              title: 'Alert',
              description:
                'College management has been notified with your check in request. Please wait for a moment until they verified your request',
            };
            this.dialogService.open(ModalComponent, { context: { modalData } });
            break;
        }
      }
    }
  }

  checkoutOnClick() {
    if (this.user.room_status === 'checked_in') {
      let modalData: ModalData = {
        title: 'Wait a minute',
        description:
          'Is this your last check-out for this year of academic session?',
        cancelButtonText: 'No',
        okButtonText: 'Yes',
      };

      this.dialogService
        .open(ModalComponent, { context: { modalData } })
        .onClose.subscribe(async (data: boolean) => {
          if (data) {
            this.dialogService
              .open(CheckOutModal)
              .onClose.subscribe(async (data: boolean) => {
                if (data) {
                  await this.checkout();
                }
              });
          }

          if (data === false) {
            await this.checkout();
          }
        });
    }
  }

  async checkout() {
    let modalData: ModalData;
    try {
      await this.dashboardService.checkout();

      modalData = {
        title: 'Check out request sent',
        description:
          'Your room check out status is waiting to be reviewed. Please wait for a moment.',
        cancelButtonText: 'Okay',
      };
      this.dialogService.open(ModalComponent, { context: { modalData } });
    } catch (error) {
      switch (error.error.messageCode) {
        case 'activity_request_fail':
          modalData = {
            title: 'Alert',
            description:
              'Application for check out is not available for you at the moment',
          };
          this.dialogService.open(ModalComponent, { context: { modalData } });
          break;

        case 'duplicate_activity':
          modalData = {
            title: 'Alert',
            description:
              'College management has been notified with your check out request. Please wait for a moment until they verified your request',
          };
          this.dialogService.open(ModalComponent, { context: { modalData } });
          break;
      }
    }
  }
}
