import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NbDialogModule, NbDialogRef } from '@nebular/theme';
import { NbDialogRefMock } from 'src/app/mocks/NbDialogRefMock';
import { UtilService } from 'src/app/shared/services/util.service';

import { CheckInModal } from './check-in.component';

describe('CheckInModal', () => {
  let component: CheckInModal;
  let fixture: ComponentFixture<CheckInModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, NbDialogModule],
      providers: [
        UtilService,
        { provide: NbDialogRef, useClass: NbDialogRefMock },
      ],
      declarations: [CheckInModal],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
