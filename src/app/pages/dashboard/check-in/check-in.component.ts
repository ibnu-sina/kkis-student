import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { SignaturePad } from 'angular2-signaturepad';
import { Upload, UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.scss'],
})
export class CheckInModal implements AfterViewInit, OnInit {
  @ViewChild('signaturePad', { static: false }) signaturePad: SignaturePad;
  signatureAvailable: boolean = false;

  signaturePadOptions = {
    canvasWidth: 270,
    canvasHeight: 300,
    minWidth: 0.5,
    maxWidth: 1.5,
  };

  signature: Upload;
  loading: boolean = true;

  constructor(
    private util: UtilService,
    protected ref: NbDialogRef<CheckInModal>
  ) {}

  async ngOnInit() {
    await this.getSignature();
    this.loading = false;
  }

  ngAfterViewInit() {
    if (this.signaturePad) {
      this.signaturePad.set('minWidth', 5);
      this.signaturePad.clear();
    }
  }

  async getSignature() {
    let signatures = await this.util.getUploads('signature');
    this.signature = signatures[0];
  }

  drawStart() {
    this.signatureAvailable = true;
  }

  async clear() {
    //delete signature from db if exist
    if (this.signature) {
      try {
        await this.util.deleteUpload(this.signature.id);
        this.signature = undefined;
      } catch (error) {}
    } else {
      this.signatureAvailable = false;
      this.signaturePad.clear();
    }
  }

  async submit() {
    //if existing signature is being used
    if (this.signature) {
      this.ref.close(true);
    }

    //if existing signature is deleted
    //and base64 image signature is being used
    if (!this.signature && this.signatureAvailable) {
      let form = new FormData();
      let image = this.signaturePad.toDataURL();
      form.append(
        'file',
        this.util.dataURLtoImage(image, new Date().getTime().toString())
      );

      try {
        await this.util.uploadFile('signature', form);
        this.ref.close(true);
      } catch (error) {}
    }
  }

  close() {
    this.ref.close();
  }
}
