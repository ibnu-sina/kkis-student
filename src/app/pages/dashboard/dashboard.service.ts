import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DashboardService {
  constructor(private http: HttpClient) {}

  async checkin() {
    try {
      await this.http.post('/student-activities/checkin', {}).toPromise();
    } catch (error) {
      throw error;
    }
  }

  async checkout() {
    try {
      await this.http.post('/student-activities/checkout', {}).toPromise();
    } catch (error) {
      throw error;
    }
  }
}
