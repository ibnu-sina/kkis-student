import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { CheckInModal } from './check-in/check-in.component';
import { CheckOutModal } from './check-out/check-out.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
];
@NgModule({
  declarations: [DashboardComponent, CheckInModal, CheckOutModal],
  imports: [SharedModule, RouterModule.forChild(routes)],
  providers: [DashboardService],
})
export class DashboardModule {}
