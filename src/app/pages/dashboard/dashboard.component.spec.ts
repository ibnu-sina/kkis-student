import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NbDialogService } from '@nebular/theme';
import { NbDialogServiceMock } from 'src/app/mocks/NbDialogServiceMock';
import { UserService } from 'src/app/shared/services/user.service';
import { UserServiceMock } from 'src/app/shared/services/user.service.spec';

import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { DashboardServiceMock } from './dashboard.service.spec';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        { provide: NbDialogService, useClass: NbDialogServiceMock },
        { provide: UserService, useClass: UserServiceMock },
        { provide: DashboardService, useClass: DashboardServiceMock },
      ],
      declarations: [DashboardComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
