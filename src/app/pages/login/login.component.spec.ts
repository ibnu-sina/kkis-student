import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule } from 'src/app/mocks/mock.module';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AuthServiceMock } from 'src/app/shared/services/auth.service.spec';
import { FormService } from 'src/app/shared/services/form.service';
import { FormServiceMock } from 'src/app/shared/services/form.service.spec';

import { LoginPage } from './login.component';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MockModule, RouterTestingModule],
      declarations: [LoginPage],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: FormService, useClass: FormServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init properly', () => {
    spyOn(component, 'createForm').and.callThrough();

    component.ngOnInit();

    expect(component.createForm).toHaveBeenCalled();
  });

  it('show/hide password when clicked', () => {
    spyOn(component, 'toggleShowPassword');
    let button = fixture.nativeElement.querySelector(
      'button#show_password_toggle'
    );
    let input = fixture.nativeElement.querySelector('input#password');

    //show password
    button.click();

    fixture.detectChanges();

    expect(component.showPassword).toBeTrue();
    expect(input.type).toEqual('text');

    //hide password
    button.click();

    fixture.detectChanges();

    expect(component.showPassword).toBeFalse();
    expect(input.type).toEqual('password');
  });

  it('check form validity', () => {
    let form = component.form;

    form.setValue({ email: '', password: '' });
    expect(form.valid).toBeFalse();

    form.setValue({ email: 'jua@email.com', password: '' });
    expect(form.valid).toBeFalse();

    form.setValue({ email: '', password: 'password' });
    expect(form.valid).toBeFalse();

    form.setValue({ email: 'jua@email.', password: 'password' });
    expect(form.valid).toBeFalse();

    form.setValue({ email: 'jua@email.com', password: 'password' });
    expect(form.valid).toBeTrue();
  });

  it('properly submit', async () => {
    spyOn(component, 'onSubmit');

    component.form.patchValue({
      email: 'juaaa@yopmail.com',
      password: 'password',
    });

    fixture.detectChanges();

    let button = fixture.nativeElement.querySelector('button#submit');

    button.click();

    await fixture.whenStable();

    expect(component.onSubmit).toHaveBeenCalled();
  });
});
