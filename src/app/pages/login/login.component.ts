import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  AuthService,
  LoginCredentials,
} from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { FormService } from 'src/app/shared/services/form.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginPage implements OnInit {
  form: FormGroup;
  errorMessage: string;
  showPassword: boolean;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private formService: FormService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [null, Validators.required],
    });
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  async onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      const data: LoginCredentials = this.form.value;
      try {
        this.loading = true;
        await this.authService.login({ ...data, user_type: 'Student' });
        await this.router.navigate(['/home']);
      } catch (err) {
        this.errorMessage = err.error.message;
      }

      this.loading = false;
    }
  }
}
