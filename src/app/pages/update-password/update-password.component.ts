import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { FormService } from 'src/app/shared/services/form.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss'],
})
export class UpdatePasswordPage implements OnInit {
  form: FormGroup;
  token: string;
  showPassword: boolean;
  showRepeat: boolean;
  errorMessage: string;
  successMessage: string;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authService: AuthService,
    private formService: FormService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
  }

  createForm() {
    this.form = this.formBuilder.group({
      password: [null, Validators.required],
      repeat: [null, Validators.required],
    });

    this.form.setValidators(
      this.formService.equalPassword('password', 'repeat')
    );
  }

  async onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      let password: string = this.form.get('password').value;
      try {
        this.loading = true;
        let respond = await this.authService.updatePassword(
          this.token,
          password
        );
        this.successMessage = respond.message;
      } catch (error) {
        this.errorMessage = error.error.message;
      }
      this.loading = false;
    }
  }
}
