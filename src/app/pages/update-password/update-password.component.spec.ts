import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule } from 'src/app/mocks/mock.module';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AuthServiceMock } from 'src/app/shared/services/auth.service.spec';
import { FormService } from 'src/app/shared/services/form.service';
import { FormServiceMock } from 'src/app/shared/services/form.service.spec';
import { UpdatePasswordPage } from './update-password.component';

describe('UpdatePasswordPage', () => {
  let component: UpdatePasswordPage;
  let fixture: ComponentFixture<UpdatePasswordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MockModule, RouterTestingModule],
      declarations: [UpdatePasswordPage],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: FormService, useClass: FormServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('dfsd', async () => {
    spyOn(component, 'onSubmit');

    let button = fixture.nativeElement.querySelector("button[type='submit']");
    expect(button).not.toBeNull();

    button.click();

    expect(component.onSubmit).toHaveBeenCalled();
  });
});
