import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Result } from 'src/app/shared/services/rest-interceptor.service';

@Injectable({
  providedIn: 'root',
})
export class MeritService {
  constructor(private http: HttpClient) {}

  async getMerits(): Promise<Merit[]> {
    try {
      const result: Result = await this.http.get('/merits/me').toPromise();
      return result.data;
    } catch (error) {}
  }

  async addMerit(data: MeritDTO): Promise<void> {
    try {
      await this.http.post('/merits', data).toPromise();
    } catch (error) {}
  }

  async editMerit(data: MeritDTO, id: number): Promise<void> {
    try {
      await this.http.put(`/merits/${id}`, data).toPromise();
    } catch (error) {}
  }

  async deleteMerit(id: number): Promise<void> {
    try {
      await this.http.delete(`/merits/${id}`).toPromise();
    } catch (error) {}
  }
}

export interface Merit {
  id?: number;
  student_id?: number;
  type?: string;
  name?: string;
  term?: string;
  position?: string;
  level?: string;
  achievement?: string;
  merit?: number;
  date?: Date;
  is_approved?: number;
  session?: string;
  attachment_id?: number;
  attachment?: Attachment;
  created_at?: Date;
  updated_at?: Date;
}

export interface MeritDTO {
  type?: string;
  name?: string;
  term?: string;
  level?: string;
  session?: string;
  position?: string;
  achievement?: string;
  date?: Date;
}

interface Attachment {
  id: number;
  url: string;
}
