import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { NbDialogServiceMock } from 'src/app/mocks/NbDialogServiceMock';
import { NbToastrServiceMock } from 'src/app/mocks/NbToastrServiceMock';
import { FormService } from 'src/app/shared/services/form.service';
import { FormServiceMock } from 'src/app/shared/services/form.service.spec';

import { MeritComponent } from './merit.component';
import { MeritService } from './merit.service';
import { MeritServiceMock } from './merit.service.spec';

describe('MeritComponent', () => {
  let component: MeritComponent;
  let fixture: ComponentFixture<MeritComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MeritComponent],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: NbDialogService, useClass: NbDialogServiceMock },
        { provide: FormService, useClass: FormServiceMock },
        { provide: MeritService, useClass: MeritServiceMock },
        { provide: NbToastrService, useClass: NbToastrServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeritComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
