import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { MeritForm } from './form/form.component';
import { Merit, MeritDTO, MeritService } from './merit.service';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import * as moment from 'moment';

@Component({
  selector: 'app-merit',
  templateUrl: './merit.component.html',
  styleUrls: ['./merit.component.scss'],
})
export class MeritComponent implements OnInit {
  form: FormGroup;
  loading: boolean;

  records = [];

  merits: Merit[] = [];
  sessions = [];
  totalMerit: number = 0;
  types = [
    { name: 'Society Position', merits: [] },
    { name: 'Society/Project Committee', merits: [] },
    { name: 'Competition/Activity', merits: [] },
    { name: 'Outbound Program', merits: [] },
  ];

  constructor(
    private dialogService: NbDialogService,
    private meritService: MeritService,
    private formBuilder: FormBuilder,
    private toastService: NbToastrService
  ) {
    this.form = this.formBuilder.group({
      session: null,
    });
  }

  async ngOnInit() {
    this.loading = true;

    await this.getMerits();
    if (this.merits.length > 0) {
      this.listenChanges();
      this.form.setValue({ session: this.sessions[0] });
    }

    this.loading = false;
  }

  listenChanges() {
    this.form.valueChanges.subscribe((value) => {
      this.sort(value.session);
    });
  }

  formatDate(date: Date): string {
    return moment(date).format('Do MMMM  YYYY');
  }

  async getMerits() {
    this.merits = await this.meritService.getMerits();
    this.sessions = this.merits
      .map((merit) => merit.session)
      .filter((value, index, self) => self.indexOf(value) === index)
      .sort();
  }

  sort(session: string) {
    let merits = this.merits.filter((merit) => merit.session === session);

    //count total merit per session
    this.totalMerit = 0;
    merits.forEach((merit) => {
      if (merit.is_approved) {
        this.totalMerit += merit.merit;
      }
    });

    //sort merit to type
    this.types.forEach((type) => {
      type.merits = [];
      merits.forEach((merit) => {
        if (merit.type === type.name) {
          type.merits.push(merit);
        }
      });
    });
  }

  addMerit() {
    this.dialogService.open(MeritForm).onClose.subscribe(async (data: any) => {
      if (data) {
        try {
          await this.meritService.addMerit(data);
          this.toastService.show(
            'Your merit application is submitted and waiting to be reviewed. You can still edit your application while waiting for it to be approved.',
            'Application sent successfully',
            { duration: 3000 }
          );
          await this.getMerits();
          this.form.patchValue({ session: data.session });
        } catch (error) {}
      }
    });
  }

  editMerit(merit: Merit) {
    this.dialogService
      .open(MeritForm, { context: { merit } })
      .onClose.subscribe(async (data: MeritDTO) => {
        if (data) {
          try {
            await this.meritService.editMerit(data, merit.id);
            this.toastService.show(
              'Your merit application is updated and waiting to be reviewed',
              'Application updated successfully'
            );
            await this.getMerits();
            this.form.patchValue({ session: data.session });
          } catch (error) {}
        }
      });
  }

  deleteMerit(merit: Merit) {
    this.dialogService
      .open(DeleteModalComponent)
      .onClose.subscribe(async (deleteApplication: boolean) => {
        if (deleteApplication) {
          try {
            await this.meritService.deleteMerit(merit.id);
            this.toastService.show(
              'Your merit application has been removed',
              'Application removed successfully'
            );
            await this.getMerits();
            if (this.sessions.length > 0) {
              this.form.setValue({
                session: this.sessions.includes(merit.session)
                  ? merit.session
                  : this.sessions[0],
              });
            }
          } catch (error) {}
        }
      });
  }
}
