import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NbDialogRef } from '@nebular/theme';
import { MockModule } from 'src/app/mocks/mock.module';
import { NbDialogRefMock } from 'src/app/mocks/NbDialogRefMock';
import { FormService } from 'src/app/shared/services/form.service';
import { FormServiceMock } from 'src/app/shared/services/form.service.spec';
import { Merit } from '../merit.service';
import { MeritForm } from './form.component';

describe('MeritForm', () => {
  let component: MeritForm;
  let fixture: ComponentFixture<MeritForm>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MeritForm],
      imports: [MockModule],
      providers: [
        { provide: FormService, useClass: FormServiceMock },
        { provide: NbDialogRef, useClass: NbDialogRefMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeritForm);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain specific controls based on type', () => {
    let form = component.form;
    let types = component.types;

    spyOn(component, 'dynamicForm').and.callThrough();

    form.patchValue({ type: types[0] });
    expect(component.dynamicForm).toHaveBeenCalled();
    expect(form.get('position')).toBeTruthy();
    expect(form.get('level')).toBeFalsy();
    expect(form.get('achievement')).toBeFalsy();
    expect(form.get('term')).toBeFalsy();
    expect(form.get('date')).toBeFalsy();

    form.patchValue({ type: types[1] });
    expect(component.dynamicForm).toHaveBeenCalled();
    expect(form.get('position')).toBeTruthy();
    expect(form.get('date')).toBeTruthy();
    expect(form.get('level')).toBeFalsy();
    expect(form.get('achievement')).toBeFalsy();
    expect(form.get('term')).toBeFalsy();

    form.patchValue({ type: types[2] });
    expect(component.dynamicForm).toHaveBeenCalled();
    expect(form.get('level')).toBeTruthy();
    expect(form.get('achievement')).toBeTruthy();
    expect(form.get('date')).toBeTruthy();
    expect(form.get('term')).toBeFalsy();
    expect(form.get('position')).toBeFalsy();

    form.patchValue({ type: types[3] });
    expect(component.dynamicForm).toHaveBeenCalled();
    expect(form.get('level')).toBeTruthy();
    expect(form.get('term')).toBeTruthy();
    expect(form.get('date')).toBeTruthy();
    expect(form.get('achievement')).toBeFalsy();
    expect(form.get('position')).toBeFalsy();
  });

  it('check merit validity', () => {
    let merit = component.form.get('merit');

    merit.setValue('n');
    expect(merit.valid).toBeFalse();

    merit.setValue(null);
    expect(merit.valid).toBeFalse();

    merit.setValue(-1);
    expect(merit.valid).toBeFalse();

    merit.setValue(0);
    expect(merit.valid).toBeFalse();

    merit.setValue(1);
    expect(merit.valid).toBeTrue();
  });

  it('should onchanges properly', async () => {
    component.form.setValue({
      session: '2017/2018',
      type: 'Outbound Program',
      name: 'Telkom Bandung, Indonesia',
      merit: 15,
      term: 'Short',
      level: 'International',
      date: new Date('1998-10-19'),
      attachment_id: 1,
    });

    expect(component.form.valid).toBeTrue();
  });
});
