import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import {
  dateOnly,
  FormService,
  numberOnly,
} from 'src/app/shared/services/form.service';
import { Merit, MeritDTO } from '../merit.service';
@Component({
  selector: 'merit-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class MeritForm implements OnInit {
  @Input() merit: Merit;
  form: FormGroup;
  nameLabel: string;
  today: Date;
  types = [
    'Society Position',
    'Society/Project Committee',
    'Competition/Activity',
    'Outbound Program',
  ];
  levels = ['International', 'National', 'State', 'University', 'College'];
  terms = ['Long', 'Short'];
  sessions = [
    '2020/2021',
    '2019/2020',
    '2018/2019',
    '2017/2018',
    '2016/2017',
    '2015/2016',
  ];

  constructor(
    private formBuilder: FormBuilder,
    protected ref: NbDialogRef<MeritForm>,
    private formService: FormService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.today = new Date();
    if (this.merit) {
      this.patchForm();
    }
    this.listenChanges();
  }

  listenChanges() {
    this.form.get('type').valueChanges.subscribe((value) => {
      this.dynamicForm(value);
    });
  }

  dynamicForm(value: string) {
    switch (value) {
      case this.types[0]:
      case this.types[1]:
        this.addControl(['position']);
        this.removeControl(['level', 'achievement', 'term']);
        this.nameLabel = 'Society/Club';
        break;

      case this.types[2]:
        this.addControl(['level', 'achievement']);
        this.removeControl(['term', 'position']);
        this.nameLabel = 'Activity';
        break;

      case this.types[3]:
        this.addControl(['term', 'level']);
        this.removeControl(['position', 'achievement']);
        this.nameLabel = 'Activity';
    }

    if (value !== this.types[0]) {
      this.form.addControl('date', this.formBuilder.control(null, dateOnly));
    }
  }

  addControl(controls: string[]) {
    for (let control of controls) {
      this.form.addControl(
        control,
        this.formBuilder.control(null, Validators.required)
      );
    }
  }

  removeControl(controls: string[]) {
    for (let control of controls) {
      this.form.removeControl(control);
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      session: [null, Validators.required],
      type: [null, Validators.required],
      name: [null, Validators.required],
      merit: [
        null,
        Validators.compose([
          Validators.required,
          Validators.min(1),
          numberOnly,
        ]),
      ],
      attachment_id: [null, Validators.required],
    });
  }

  patchForm() {
    this.dynamicForm(this.merit.type);

    let date = new Date(this.merit.date);
    this.form.patchValue({ ...this.merit, date });
  }

  selectAttachment(event: any) {
    let attachment_id = event;
    this.form.patchValue({ attachment_id });
  }

  onSubmit() {
    this.formService.markAllDirty(this.form);

    if (this.form.valid) {
      let { date, ...data }: MeritDTO = this.form.value;
      date = this.formService.formatDate(date);

      this.ref.close({ date, ...data });
    }
  }

  close() {
    this.ref.close();
  }
}
