import { NgModule } from '@angular/core';

import { MeritComponent } from './merit.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { MeritForm } from './form/form.component';

const routes: Routes = [
  {
    path: '',
    component: MeritComponent,
  },
];

@NgModule({
  declarations: [MeritComponent, MeritForm],
  imports: [SharedModule, RouterModule.forChild(routes)],
  entryComponents: [MeritComponent],
})
export class MeritModule {}
