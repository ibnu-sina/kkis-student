import { TestBed } from '@angular/core/testing';

import { Merit, MeritDTO, MeritService } from './merit.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('MeritService', () => {
  let service: MeritService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MeritService],
    });
    service = TestBed.inject(MeritService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class MeritServiceMock {
  async getMerits(): Promise<Merit[]> {
    return undefined;
  }
  async addMerit(data: MeritDTO): Promise<void> {}
  async editMerit(data: MeritDTO, id: number): Promise<void> {}
  async deleteMerit(id: number): Promise<void> {}
}
