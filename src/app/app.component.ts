import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { AuthService } from './shared/services/auth.service';
import { LocalStorageService } from './shared/services/local-storage.service';
import { Router, NavigationEnd } from '@angular/router';
import { Result } from './shared/services/rest-interceptor.service';
import { UserService } from './shared/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private userService: UserService
  ) {}

  async ngOnInit() {
    const isLoggedIn = await this.localStorageService.getItem<boolean>(
      'isLoggedIn'
    );
    const refreshToken = await this.localStorageService.getItem<string>(
      'refreshToken'
    );

    if (isLoggedIn) {
      try {
        const result: Result = await this.authService
          .refreshToken(refreshToken)
          .toPromise();

        await Promise.all([
          this.localStorageService.setItem<string>('token', result.data.token),
          this.localStorageService.setItem<string>(
            'refreshToken',
            result.data.refreshToken
          ),
        ]);

        await this.userService.getUser();
        await this.authService.startTokenRenewal();
      } catch (error) {
        await this.authService.clearStorage();
        await this.router.navigate(['login']);
      }
    }

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
