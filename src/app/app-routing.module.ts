import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeauthGuard } from './shared/guards/deauth.guard';
import { AuthGuard } from './shared/guards/auth.guard';
import { PageNotFound } from './shared/components/page-not-found/page-not-found.component';
import { ForgotPasswordPage } from './pages/forgot-password/forgot-password.component';
import { LoginPage } from './pages/login/login.component';
import { UpdatePasswordPage } from './pages/update-password/update-password.component';
import { SignupPage } from './pages/signup/signup.component';
import { LayoutComponent } from './shared/layout/layout.component';

const routes: Routes = [
  {
    path: 'login',
    canLoad: [DeauthGuard],
    canActivate: [DeauthGuard],
    component: LoginPage,
  },
  {
    path: 'forgot-password',
    canLoad: [DeauthGuard],
    canActivate: [DeauthGuard],
    component: ForgotPasswordPage,
  },
  {
    path: 'update-password/:token',
    canLoad: [DeauthGuard],
    canActivate: [DeauthGuard],
    component: UpdatePasswordPage,
  },
  {
    path: 'register/:token',
    canLoad: [DeauthGuard],
    canActivate: [DeauthGuard],
    component: SignupPage,
  },
  {
    path: '',
    component: LayoutComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./pages/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('./pages/profile/profile.module').then((m) => m.ProfileModule),
      },
      {
        path: 'settings',
        loadChildren: () =>
          import('./pages/security/security.module').then(
            (m) => m.SecurityModule
          ),
      },
      {
        path: 'merit',
        loadChildren: () =>
          import('./pages/merit/merit.module').then((m) => m.MeritModule),
      },
      {
        path: '',
        loadChildren: () =>
          import('./pages/paperwork/paperwork.module').then(
            (m) => m.PaperworkModule
          ),
      },
      {
        path: 'file-manager',
        loadChildren: () =>
          import('./pages/upload/upload.module').then((m) => m.UploadModule),
      },
    ],
  },
  {
    path: 'sandbox-bdac8d38-0ba7-4f19-8cdf-c60f77a40470',
    loadChildren: () =>
      import('./pages/sandbox/sandbox.module').then((m) => m.SandboxModule),
  },
  {
    path: 'page-not-found',
    component: PageNotFound,
  },
  {
    path: '**',
    redirectTo: 'page-not-found',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
