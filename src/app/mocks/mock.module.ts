import { HttpClientTestingModule } from '@angular/common/http/testing';

import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NbButtonModule,
  NbDatepickerModule,
  NbInputModule,
  NbSpinnerModule,
  NbThemeModule,
} from '@nebular/theme';

@NgModule({
  imports: [
    HttpClientTestingModule,
    ReactiveFormsModule,
    NbThemeModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbInputModule,
    NbButtonModule,
    NbSpinnerModule,
  ],
  exports: [
    HttpClientTestingModule,
    ReactiveFormsModule,
    NbThemeModule,
    NbDatepickerModule,
    NbInputModule,
    NbButtonModule,
    NbSpinnerModule,
  ],
})
export class MockModule {}
