import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Router,
} from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private router: Router,
    private localStorageService: LocalStorageService
  ) {}

  canActivate(): Promise<boolean> {
    return this.checkLogin();
  }

  canActivateChild(): Promise<boolean> {
    return this.checkLogin();
  }

  canLoad(): Promise<boolean> {
    return this.checkLogin();
  }

  async checkLogin(): Promise<boolean> {
    const isLoggedIn = await this.localStorageService.getItem<boolean>(
      'isLoggedIn'
    );

    if (isLoggedIn) {
      return true;
    } else {
      await this.router.navigate(['login']);
      return false;
    }
  }
}
