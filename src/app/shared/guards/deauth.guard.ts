import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanLoad,
  Router,
  CanActivateChild,
} from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class DeauthGuard implements CanActivate, CanLoad, CanActivateChild {
  constructor(
    private localStorageService: LocalStorageService,
    private router: Router
  ) {}

  canActivate(): Promise<boolean> {
    return this.checkLogin();
  }
  canActivateChild(): Promise<boolean> {
    return this.checkLogin();
  }
  canLoad(): Promise<boolean> {
    return this.checkLogin();
  }

  async checkLogin(): Promise<boolean> {
    const isLoggedIn = await this.localStorageService.getItem<boolean>(
      'isLoggedIn'
    );

    if (isLoggedIn) {
      await this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
