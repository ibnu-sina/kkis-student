import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss'],
})
export class DeleteModalComponent implements OnInit {
  constructor(protected ref: NbDialogRef<DeleteModalComponent>) {}

  ngOnInit() {}

  delete() {
    this.ref.close(true);
  }

  cancel() {
    this.ref.close(false);
  }
}
