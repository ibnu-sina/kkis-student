import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  modalData: ModalData;
  defaultData: ModalData = {
    cancelButtonText: 'Okay',
    onClose: () => this.close(),
    onOkay: () => this.okay(),
  };

  constructor(protected ref: NbDialogRef<ModalComponent>) {}

  ngOnInit() {
    this.modalData = { ...this.defaultData, ...this.modalData };
  }

  close() {
    this.ref.close(false);
  }

  okay() {
    this.ref.close(true);
  }
}

export interface ModalData {
  title?: string;
  description?: string;
  cancelButtonText?: string;
  onClose?: () => any;
  okButtonText?: string;
  onOkay?: () => any;
}
