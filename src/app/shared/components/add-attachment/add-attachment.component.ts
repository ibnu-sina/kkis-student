import { Component, Input, OnInit } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';

@Component({
  selector: 'app-add-attachment',
  templateUrl: './add-attachment.component.html',
  styleUrls: ['./add-attachment.component.scss'],
})
export class AddAttachmentComponent implements OnInit {
  @Input() uploadOption: UploadOption;
  option: UploadOption = {
    multiple: false,
  };
  files: File[] = [];
  showError: boolean;

  constructor(
    private ref: NbDialogRef<AddAttachmentComponent>,
    private toastService: NbToastrService
  ) {}

  ngOnInit() {
    this.option = { ...this.option, ...this.uploadOption };
  }

  public dropped(files: NgxFileDropEntry[]) {
    if (this.option.multiple) {
      for (const droppedFile of files) {
        if (
          droppedFile.fileEntry.isFile &&
          this.isFileAllowed(droppedFile.fileEntry.name)
        ) {
          let fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
          fileEntry.file((file: File) => {
            this.files.push(file);
          });
        } else {
          this.showError = true;
        }
      }
    } else {
      let droppedFile = files[0];
      if (
        droppedFile.fileEntry.isFile &&
        this.isFileAllowed(droppedFile.fileEntry.name)
      ) {
        let fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.files = [];
          this.files.push(file);
        });
      } else {
        this.showError = true;
      }
    }
  }

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    const allowedFiles = ['.pdf', '.jpg', '.jpeg', '.png'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    return isFileAllowed;
  }

  deleteFile(id: number) {
    this.files.splice(id, 1);
  }

  async submit() {
    this.ref.close(this.files);
  }
}

export interface UploadOption {
  multiple?: boolean;
  accept?: string;
  uploadType?: string;
}
