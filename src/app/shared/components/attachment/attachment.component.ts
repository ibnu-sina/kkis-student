import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Upload, UtilService } from '../../services/util.service';

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss'],
})
export class AttachmentComponent implements OnChanges, OnInit {
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @Output() selectAttachment = new EventEmitter<number>();
  @Input() type: string;
  @Input() selectedAttachment: number;
  @Input() errorMessage: boolean;

  uploads: Upload[] = [];
  form: FormGroup;
  loading: boolean;
  uploading: boolean;

  constructor(private util: UtilService, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      file: [null, Validators.required],
    });
  }

  async ngOnInit() {
    if (this.type) {
      this.loading = true;
      await this.getUploads();
      this.loading = false;
    }
  }

  async ngOnChanges(changes: SimpleChanges) {
    // if (changes.type.currentValue) {
    //   this.loading = true;
    //   await this.getUploads();
    //   this.loading = false;
    // }
  }

  async getUploads() {
    this.uploads = await this.util.getUploads(this.type);
  }

  formatName(upload: any): string {
    let path: string = upload.path;
    path = path.replace(`${this.type}/student_${upload.student_id}_`, '');
    return path;
  }

  async onSubmit() {
    if (this.form.valid) {
      let form = new FormData();
      form.append('file', this.fileInput.nativeElement.files[0]);

      try {
        this.uploading = true;
        await this.util.uploadFile('merit', form);
        await this.getUploads();
        this.form.patchValue({ file: null });
      } catch (error) {}

      this.uploading = false;
    }
  }

  onSelect(id?: number) {
    this.errorMessage = undefined;
    if (this.selectedAttachment === id) {
      this.selectedAttachment = undefined;
    } else {
      this.selectedAttachment = id;
    }

    this.selectAttachment.emit(this.selectedAttachment);
  }
}
