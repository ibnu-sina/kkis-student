import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawSignatureModal } from './draw-signature.component';

describe('DrawSignatureModal', () => {
  let component: DrawSignatureModal;
  let fixture: ComponentFixture<DrawSignatureModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DrawSignatureModal],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawSignatureModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
