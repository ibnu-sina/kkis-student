import { ViewChild } from '@angular/core';
import { AfterViewInit, Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { SignaturePad } from 'angular2-signaturepad';
import { UtilService } from '../../services/util.service';

@Component({
  selector: 'app-draw-signature',
  templateUrl: './draw-signature.component.html',
  styleUrls: ['./draw-signature.component.scss'],
})
export class DrawSignatureModal implements AfterViewInit {
  @ViewChild('signaturePad', { static: false }) signaturePad: SignaturePad;
  signatureAvailable: boolean = false;

  signaturePadOptions = {
    canvasWidth: 270,
    canvasHeight: 300,
    minWidth: 0.5,
    maxWidth: 1.5,
  };

  constructor(
    private util: UtilService,
    protected ref: NbDialogRef<DrawSignatureModal>
  ) {}

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 5);
    this.signaturePad.clear();
  }

  drawStart() {
    this.signatureAvailable = true;
  }

  clear() {
    this.signaturePad.clear();
  }

  submit() {
    if (this.signatureAvailable) {
      let form = new FormData();
      let image = this.signaturePad.toDataURL();
      form.append(
        'file',
        this.util.dataURLtoImage(image, new Date().getTime().toString())
      );

      this.ref.close(form);
    }
  }

  close() {
    this.ref.close();
  }
}
