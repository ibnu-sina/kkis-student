import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { HeaderComponent } from './layout/header/header.component';
import { ModalComponent } from './components/modal/modal.component';
import { DeleteModalComponent } from './components/delete-modal/delete-modal.component';
import { PageNotFound } from './components/page-not-found/page-not-found.component';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password.component';
import { LoginPage } from '../pages/login/login.component';
import { UpdatePasswordPage } from '../pages/update-password/update-password.component';
import { SignupPage } from '../pages/signup/signup.component';
import { AttachmentComponent } from './components/attachment/attachment.component';
import {
  NbLayoutModule,
  NbStepperModule,
  NbCardModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbOptionModule,
  NbDatepickerModule,
  NbSidebarModule,
  NbMenuModule,
  NbIconModule,
  NbUserModule,
  NbContextMenuModule,
  NbActionsModule,
  NbAccordionModule,
  NbCheckboxModule,
  NbTreeGridModule,
  NbDialogModule,
  NbAlertModule,
  NbSpinnerModule,
  NbRadioModule,
  NbTabsetModule,
} from '@nebular/theme';
import { LayoutComponent } from './layout/layout.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { DrawSignatureModal } from './components/draw-signature/draw-signature.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { AddAttachmentComponent } from './components/add-attachment/add-attachment.component';

@NgModule({
  declarations: [
    HeaderComponent,
    ModalComponent,
    DeleteModalComponent,
    PageNotFound,
    LoginPage,
    ForgotPasswordPage,
    UpdatePasswordPage,
    SignupPage,
    AttachmentComponent,
    LayoutComponent,
    DrawSignatureModal,
    AddAttachmentComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbOptionModule,
    NbDatepickerModule,
    NbSidebarModule,
    NbMenuModule,
    NbIconModule,
    NbUserModule,
    NbContextMenuModule,
    NbActionsModule,
    NbAccordionModule,
    NbCheckboxModule,
    NbTreeGridModule,
    NbDialogModule,
    NbAlertModule,
    NbSpinnerModule,
    NbRadioModule,
    NbTabsetModule,
    NgxFileDropModule,
    SignaturePadModule,
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbOptionModule,
    NbDatepickerModule,
    NbSidebarModule,
    NbMenuModule,
    NbIconModule,
    NbUserModule,
    NbContextMenuModule,
    NbActionsModule,
    NbAccordionModule,
    NbCheckboxModule,
    NbTreeGridModule,
    NbDialogModule,
    NbAlertModule,
    AttachmentComponent,
    NbSpinnerModule,
    NbRadioModule,
    NbTabsetModule,
    NgxFileDropModule,
    SignaturePadModule,
    DrawSignatureModal,
    AddAttachmentComponent,
  ],
  entryComponents: [ModalComponent, DeleteModalComponent],
})
export class SharedModule {}
