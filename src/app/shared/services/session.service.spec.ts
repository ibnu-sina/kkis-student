import { TestBed } from '@angular/core/testing';

import { SessionService } from './session.service';
import { User } from './user.service';

describe('SessionService', () => {
  let service: SessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionService],
    });
    service = TestBed.inject(SessionService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class SessionServiceMock {
  user: User;
}
