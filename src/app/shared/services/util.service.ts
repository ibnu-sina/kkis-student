import { Injectable } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Result } from './rest-interceptor.service';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  constructor(private http: HttpClient) {}

  formatPrice(price: number): string {
    if (!price) {
      price = 0;
    }
    return 'RM' + new DecimalPipe('en-US').transform(price, '1.2-2');
  }

  async uploadFile(type: string, form: FormData) {
    try {
      await this.http.post(`/student-uploads/${type}`, form).toPromise();
    } catch (error) {}
  }

  async getUploads(type: string): Promise<Upload[]> {
    try {
      const result: Result = await this.http
        .get(`/student-uploads/${type}`)
        .toPromise();
      return result.data;
    } catch (error) {}
  }

  async deleteUpload(id: number): Promise<void> {
    try {
      await this.http.delete(`/student-uploads/${id}`).toPromise();
    } catch (error) {
      throw error;
    }
  }

  dataURLtoImage(dataurl: string, filename: string): File {
    //Dataurl is 'data:image/jpeg;base64, ' + base64_string
    let arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while (n--) u8arr[n] = bstr.charCodeAt(n);
    return new File([u8arr], filename, { type: mime });
  }

  jsonToFD(data: Object): FormData {
    let form = new FormData();

    Object.keys(data).forEach((key) => {
      form.append(key, data[key]);
    });
    return form;
  }

  appendImgsToFD(
    form: FormData,
    images: string[],
    image_key: string
  ): FormData {
    images.forEach((image, index) => {
      let imageFile = this.dataURLtoImage(image, index.toString());
      form.append(`${image_key}[${index.toString()}]`, imageFile);
    });

    return form;
  }

  appendImgToFD(form: FormData, image?: string, image_key?: string): FormData {
    let imageFile = this.dataURLtoImage(image, 'image');
    form.append(`${image_key}`, imageFile);

    return form;
  }
}

export interface Upload {
  id?: number;
  path?: string;
  url?: string;
  type?: string;
  student_id?: number;
  created_at?: Date;
  updated_at?: Date;
}
