import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class OfflineService {
  constructor(private http: HttpClient, private loaderService: LoaderService) {}

  async getData<T>(filename: string = 'postprototype.json'): Promise<T> {
    this.loaderService.show(filename);
    const response = <any>(
      await this.http.get(`assets/data/${filename}`).toPromise()
    );
    this.loaderService.hide(filename);

    const { errorcode } = response;
    if (typeof errorcode === 'number') {
      let massagedResponse = {
        ...response,
        errorcode: errorcode.toString()
      };

      return massagedResponse;
    }

    return response;
  }
}
