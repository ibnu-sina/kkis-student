import { Injectable } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  constructor() {}

  markAllDirty(control: AbstractControl) {
    if (control.hasOwnProperty('controls')) {
      control.markAsDirty(); // mark group
      let ctrl = <any>control;
      for (let inner in ctrl.controls) {
        this.markAllDirty(ctrl.controls[inner] as AbstractControl);
      }
    } else {
      (<FormControl>control).markAsDirty();
    }
  }

  equalPassword(valOne: string, valTwo: string) {
    return (control: AbstractControl): { equalPassword: boolean } => {
      if (control.get(valOne) === null || control.get(valTwo) === null) {
        return null;
      }
      if (control.get(valOne).value !== control.get(valTwo).value) {
        return { equalPassword: true };
      } else {
        return null;
      }
    };
  }

  formatDate(date: Date): Date {
    return new Date(moment(date).format('YYYY-MM-DD') + 'T00:00:00.000Z');
  }
}

export function numberOnly(control: AbstractControl): ValidationErrors | null {
  return /\D/.test(control.value)
    ? { numberOnly: { value: control.value } }
    : null;
}

export function dateOnly(control: AbstractControl): ValidationErrors | null {
  if (Object.prototype.toString.call(control.value) !== '[object Date]') {
    return { dateOnly: true };
  } else {
    return null;
  }
}
