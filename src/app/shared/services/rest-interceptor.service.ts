import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { LocalStorageService } from './local-storage.service';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { NbDialogService } from '@nebular/theme';
import { ModalData, ModalComponent } from '../components/modal/modal.component';

@Injectable({
  providedIn: 'root',
})
export class RestInterceptorService implements HttpInterceptor {
  constructor(
    private environmentService: EnvironmentService,
    private localStorageService: LocalStorageService,
    private dialogService: NbDialogService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Prefix the baseUrl
    const url = `${this.environmentService.get('baseUrl')}${req.url}`;
    req = req.clone({ url });

    const token$ = from(this.localStorageService.getItem<string>('token'));

    return token$.pipe(
      switchMap((token) => {
        if (token) {
          req = req.clone({
            headers: new HttpHeaders({
              Authorization: `Bearer ${token}`,
            }),
          });
        }
        return next.handle(req);
      }),
      catchError((error: HttpErrorResponse) => {
        let modalData: ModalData;
        let showModal: boolean;

        switch (error.status) {
          case 500:
            showModal = true;
            modalData = {
              title: 'Server Error',
              description:
                "We're sorry! The server has encountered an internal error and was unable to complete your request. Please contact the system administrator for more information.",
            };
        }
        if (showModal) {
          this.dialogService.open(ModalComponent, { context: { modalData } });
        }
        return throwError(error);
      })
    );
  }
}

export interface Result {
  message?: string;
  data?: any;
  page?: number;
  lastPage?: number;
  perPage?: number;
  total?: number;
}
