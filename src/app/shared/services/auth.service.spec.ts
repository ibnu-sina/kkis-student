import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';

import {
  AuthService,
  LoginCredentials,
  RegisterStudentDTO,
  SignupTokenResponse,
} from './auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService],
    });
    service = TestBed.inject(AuthService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class AuthServiceMock {
  async registerStudent(data?: RegisterStudentDTO): Promise<void> {}
  async login(loginCredentials?: LoginCredentials): Promise<void> {}
  async logout(): Promise<void> {}
  async clearStorage(): Promise<void> {}
  async forgotPassword(email: string): Promise<any> {
    return undefined;
  }
  async updatePassword(token: string, password: string): Promise<any> {
    return undefined;
  }
  async changePassword(data: {
    old_password: string;
    password: string;
  }): Promise<void> {}
  async checkSignupToken(token: string): Promise<SignupTokenResponse> {
    return undefined;
  }
  async signup(data: {
    password: string;
    confirm_password: string;
    token: string;
  }): Promise<void> {}
  //token management
  refreshToken(refreshToken: string): Observable<any> {
    return undefined;
  }
  async startTokenRenewal(): Promise<void> {}
  stopTokenRenewal(): void {}
}
