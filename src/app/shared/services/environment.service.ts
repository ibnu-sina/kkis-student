import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentService {
  constructor() {}

  get<T>(key: keyof EnvironmentData): T {
    return <any>environment[key];
  }

  set<T>(key: keyof EnvironmentData, value: T) {
    (<any>environment)[key] = value;
  }
}

export interface EnvironmentData {
  production: boolean;
  baseUrl: string;
  online: boolean;
}
