import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EnvironmentService } from './environment.service';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  loaderArray: string[] = [];
  sub = new BehaviorSubject([]);
  obs = this.sub.asObservable();
  constructor(private environmentService: EnvironmentService) {}

  show(api: string) {
    this.loaderArray.push(api);
    this.sub.next(null);
  }

  hide(api: string) {
    if (api) api = api.replace(this.environmentService.get('baseUrl'), '');
    this.loaderArray = this.loaderArray.filter((obj) => obj !== api);
    this.sub.next(null);
  }
}
