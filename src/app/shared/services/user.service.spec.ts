import { TestBed } from '@angular/core/testing';

import { User, UserService } from './user.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('UserService', () => {
  let service: UserService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService],
    });
    service = TestBed.inject(UserService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });

  // it('should get user', () => {
  //   service.getUser();

  //   const mockRequest = httpTestingController.expectOne('/students/me');

  //   expect(mockRequest.cancelled).toBeFalsy();
  //   expect(mockRequest.request.responseType).toEqual('json');
  //   httpTestingController.verify();
  // });
});

export class UserServiceMock {
  async getUser(): Promise<User> {
    return undefined;
  }
  async editUser(data: User): Promise<void> {}
}
