import { Injectable } from '@angular/core';
import * as localforage from 'localforage';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {
    localforage.config({
      name: 'avicenna-data',
    });
  }

  async setItem<T>(key: keyof LocalStorageItems, value: T): Promise<T> {
    return localforage.setItem(key, value);
  }

  async getItem<T>(key: keyof LocalStorageItems): Promise<T> {
    return localforage.getItem(key);
  }

  async removeItem(key: keyof LocalStorageItems): Promise<void> {
    return localforage.removeItem(key);
  }

  async clear(): Promise<void> {
    return localforage.clear();
  }
}

export interface LocalStorageItems {
  token: string;
  refreshToken: string;
  isLoggedIn: boolean;
}
