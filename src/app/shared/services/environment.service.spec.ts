import { TestBed } from '@angular/core/testing';

import { EnvironmentData, EnvironmentService } from './environment.service';

describe('EnvironmentService', () => {
  let service: EnvironmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnvironmentService],
    });
    service = TestBed.inject(EnvironmentService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class EnvironmentServiceMock {
  get(key: keyof EnvironmentData): any {}
  set(key: keyof EnvironmentData, value: any) {}
}
