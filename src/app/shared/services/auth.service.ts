import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription, interval } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Result } from './rest-interceptor.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  tokenRenewalSubscription: Subscription;

  constructor(
    private localStorageService: LocalStorageService,
    private http: HttpClient
  ) {}

  async login(data: LoginCredentials) {
    try {
      const result: Result = await this.http
        .post('/auth/login', data)
        .toPromise();

      await Promise.all([
        this.localStorageService.setItem('isLoggedIn', true),
        this.localStorageService.setItem('token', result.data.token),
        this.localStorageService.setItem(
          'refreshToken',
          result.data.refreshToken
        ),
      ]);

      this.startTokenRenewal();
    } catch (error) {
      throw error;
    }
  }

  async logout() {
    this.stopTokenRenewal();

    const data = {
      user_type: 'Student',
      refreshToken: await this.localStorageService.getItem<string>(
        'refreshToken'
      ),
    };

    try {
      await this.http.post('/auth/logout', data).toPromise();
      await this.clearStorage();
    } catch (error) {
      throw error;
    }
  }

  async clearStorage() {
    await Promise.all([
      this.localStorageService.removeItem('token'),
      this.localStorageService.removeItem('refreshToken'),
      this.localStorageService.setItem('isLoggedIn', false),
    ]);
  }

  async forgotPassword(email: string): Promise<void> {
    const data = { email, user_type: 'Student' };

    try {
      await this.http.post('/auth/forgot-password', data).toPromise();
    } catch (error) {
      throw error;
    }
  }

  async updatePassword(token: string, password: string): Promise<any> {
    const data = { token, password, user_type: 'Student' };
    try {
      return await this.http.post('/auth/update-password', data).toPromise();
    } catch (error) {
      throw error;
    }
  }

  async changePassword(data: { old_password: string; password: string }) {
    try {
      return await this.http.post('/auth/change-password', data).toPromise();
    } catch (error) {
      throw error;
    }
  }

  async checkSignupToken(token: string): Promise<SignupTokenResponse> {
    try {
      const result: Result = await this.http
        .get(`/student-invites/${token}`)
        .toPromise();
      return result.data;
    } catch (error) {
      throw error;
    }
  }

  async signup(data: RegisterStudentDTO) {
    try {
      await this.http.post('/students', data).toPromise();
    } catch (error) {
      throw error;
    }
  }

  //token management
  refreshToken(refreshToken: string): Observable<any> {
    return this.http.post<any>('/auth/refresh-token', {
      user_type: 'Student',
      refreshToken: refreshToken,
    });
  }

  async startTokenRenewal() {
    const refreshToken: string = await this.localStorageService.getItem<string>(
      'refreshToken'
    );
    this.tokenRenewalSubscription = interval(1000 * 60 * 60 * 24)
      .pipe(switchMap(() => this.refreshToken(refreshToken)))
      .subscribe(
        async (result) => {
          await Promise.all([
            this.localStorageService.setItem('token', result.data.token),
            this.localStorageService.setItem(
              'refreshToken',
              result.data.refreshToken
            ),
          ]);
        },
        (error) => {
          console.log('Token could not be renewed', error);
        }
      );
  }

  stopTokenRenewal() {
    if (this.tokenRenewalSubscription) {
      this.tokenRenewalSubscription.unsubscribe();
    }
  }
}

export interface LoginCredentials {
  email: string;
  password: string;
  user_type: string;
}

export interface TokenResponse {
  type: string;
  token: string;
  refreshToken: string;
}

export interface FTLCredentials {
  matric_no: string;
  ic_no: string;
}

export interface SignupTokenResponse {
  id: number;
  name: string;
  email: string;
  matric_no: string;
  course_id: number;
  room_id: number;
  token: string;
  expired_at: Date;
  is_active: number;
  created_at: Date;
  updated_at: Date;
}

export interface RegisterStudentDTO {
  token: string;
  credential: {
    email: string;
    password: string;
    password_confirmation: string;
  };
  student: {
    full_name: string;
    ic_no: string;
    religion: string;
    race: string;
    gender: string;
    birth_date: string;
    birth_place: string;
    matric_no: string;
    faculty: string;
    course: string;
    phone_no: string;
  };
  guardian: {
    full_name: string;
    phone_no: string;
  };
  address: {
    address_1: string;
    address_2: string;
    city: string;
    state: string;
    posscode: string;
    country: string;
  };
}
