import { TestBed } from '@angular/core/testing';

import {
  LocalStorageItems,
  LocalStorageService,
} from './local-storage.service';

describe('LocalStorageService', () => {
  let service: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class LocalStorageServiceMock {
  async setItem<T>(key: keyof LocalStorageItems, value: T): Promise<T> {
    return undefined;
  }
  async getItem<T>(key: keyof LocalStorageItems): Promise<T> {
    return undefined;
  }
  async removeItem(key: keyof LocalStorageItems): Promise<void> {}
  async clear(): Promise<void> {}
}
