import { Injectable } from '@angular/core';
import { User } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  user: User;

  constructor() {}
}
