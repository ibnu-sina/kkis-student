import { TestBed } from '@angular/core/testing';
import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';

import { FormService } from './form.service';

describe('FormService', () => {
  let service: FormService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormService],
    });
    service = TestBed.inject(FormService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});

export class FormServiceMock {
  markAllDirty(control: AbstractControl): void {}
  equalPassword(valOne: string, valTwo: string): ValidatorFn {
    return undefined;
  }
  formatDate(date: Date): Date {
    return undefined;
  }
}
