import { TestBed } from '@angular/core/testing';

import { Upload, UtilService } from './util.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UtilService', () => {
  let service: UtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UtilService],
    });
    service = TestBed.inject(UtilService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });

  // it('format price', () => {
  //   expect(service.formatPrice(1.2)).toEqual('RM1.20');
  //   expect(service.formatPrice(1)).toEqual('RM1.00');
  //   expect(service.formatPrice(1.23)).toEqual('RM1.23');
  //   expect(service.formatPrice(1.234)).toEqual('RM1.23');
  // });

  // it('convert json to formdata', () => {
  //   let data: Object = {
  //     name: 'Foo',
  //     age: 8,
  //   };

  //   let formData = service.jsonToFD(data);
  //   expect(formData.has('name')).toBeTruthy();
  //   expect(formData.has('age')).toBeTruthy();
  //   expect(formData.has('job')).toBeFalsy();
  // });

  // it('convert dataurl to image', () => {
  //   let dataurl =
  //     'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ4AAAEsCAYAAAAy1V2YAAASPklEQVR4Xu2dW6h1VRXH/9JrN6XQqFApwRTULKioMCPonlr4EVGUXQyk0ujy0ENm9RAFaYGEIWVBLxrdsB4CU+kGkmXElw8ZdqESLMpQCCOMoXvB9rjPOWPsPcfac479W3D4zvedOeeY8zfm+X17rTXXXMeIAwIQgECQwDHB8hSHAAQgIMTBJIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAAcTBHIAABMIEEEcYGRUgAAHEwRyAAATCBBBHGBkVIAABxMEcgAAEwgQQRxgZFSAAgWriOEPScZJ+JulB0gsBCOQQqCCOF0h6taR3S3r6EqZ7JV0r6WM56GgVArtLYHRxvFPS5xafMvbL4l2STtndFDNyCLQnMLI4virpHU4kd0o6zVmWYhCAwCEERhXHxZKuCWb3BklHgnUoDgEIrCAwojjsk8PRNbN5nqTvrVmXahCAwILAiOK4UtJla2bwp5JesmZdqkEAAgOL4x+HXAw9LLkXSbrusEL8HAIQ2J/AaJ84XrRYo7FJTm+X9PxNGqAuBHadwGji+KCkzzdI2hWSPtGgHZqAwE4SGE0cV0u6pFGmPi7pU43aohkI7BSB0cTxDUlvaZShhyS9UdJ3GrVHMxDYGQKjicNOLy5vmJ0HFndZ7mjYJk1BoDyB0cTxNEl/bZyVf0k6VxLyaAyW5uoSGE0clokvSnp/45Qgj8ZAaa42gRHF8TJJNyekxeRhazy45pEAlyZrERhRHJaBGyW9NikVLBBLAkuzdQiMKo6sTx1TZu0irK314IAABFYQGFUcNpSWazpWTQ5blm6fPjggAIE9BEYWhw3lekkXJmbV7rTYHRe7/sEBAQgsCIwuDhuGLUG3pehZB/LIIku7wxKoIA6Db3dZ7LpH1oE8ssjS7pAEqojD4LdeVbo3ochjyClOpzMIVBKH8bHrHXbdI+sweTw3q3HahcAoBKqJw7jb1oI3STohKQm3LC6YJjVPsxDon0BFcUzU7ZkWe7Yl4/jCBtsXZvSHNiEwK4HK4jCQP07cY9Tu5Fw1a7YIBoFOCFQXh2G2hVxvT+L9Vkm2RwgHBHaKwC6IwxKadcfFNgM6Z/HJZqcmDoPdbQK7Ig7Lsr31zd7+1vq4W9LZrC5tjZX2eiawS+KwPJy/OHV5UuOkcKelMVCa65vAronDsnGWJPtFby0P7rT0PdfpXUMCuygOw/fkhTzObMjSmmIvj8ZAaa5PArsqjikbdjv10oapYQvChjBpql8Cuy6OjOsetnP66ZL+2G/a6RkENiOAOB7hZ6cutteo3VptdfxJ0o8Wp0S/Zhf1VlhppwcCiOPRWbhM0pVJibHTGHtIzi7M2pfJhA2CkmDTbC4BxPFYvnbXxR6SOy4X/cOt/2HxZSKZvr91hriEgMBGBBDHanx26nKbpFM2ort+5enTySST6U+7bmLfc/RNwOaP3bGzP+0/Ijumjabs79Ndva8t1hX1PZoVvUMcB6fMlqp/QNKxnWV2Eomd+kzf/1fSDzrrZ9Xu2C+/rQM6afE1CWL6e2TcQ26KjTgOT7Ht73H08GLdlPi3pF8urp8si8U6yGnQ6jRNIrCfLn9KWP5+HSl4J8VwT1ojDl9qj5f0mcXzLr4afZcyoUwXZqdPLMs9tp/Zvy9fvL1vUcDq2i9U6wu7U5v2Czp9b396V/hO9azO3mM6PZj+PVMC62TeWJ+8TsVt1UEcMfJ2nmoLxuyZF47HEjCZ2C+u/SIsy8V+UTkOJmCnw61lnMYccayH9omSPiTpiKRT12uCWhB4FIGhfheH6mynEy1zo6BOh0y3GhOwu2VDfSpDHJvPgKwH5jbvGS2MQuCCxcrlUforxNEmVfa/hV009F7Is6j3S3p8m/C0MjABW8thm0wNdSCOdumyC6f2RjnvYRcQz1usULW605X+5VuD3rYoNxaBaSGfPZ1tz0gNdyCOtimL7m160OY/JhM7DVoWyrQisW2vaa01gUkMe1f+Lt8Gbx1z1vYQR3vc9txJ5CnbcxcPvUV6srxS8VULudhpj93teWakIcqGCdh6FhOAHZMIllfyDnNLNTzypQqIYxN6q+tO6xi81zts0tlrJVtOuL3PRUwLoDgNenTO7pUevs73P0l3Lb6mZ4GWF8atWhDXfuYM1CLiyEmWLRD7dqDpufcrnU6BplWW+626tJ/b13MkPUXS4yTdI+k/S6dRNkyvJJeRTCtRl1eoWoypTxbHLiAvH/YLvCzYvX/fi3zvLzwCCEzKg4oijkYgVzQT3ZZwnVOWvN7TMgQOIIA48qaH/c9p58AnOkMM97yCc1wUK0gAceQmNXqL9orFW+dye0XrENiQAOLYEKCjevSUxS6UTlftHc1TBALzE0Ac+cyjpyy8FS4/J0TYkADi2BCgs3r0lGW4jV2cHChWhADimC+Rkado7bahnbKwv+h8+SFSgADiCMDasGh0YRinLBsCp3oeAcSRx3ZVy9GFYcM9bj0vTqJtiwDimJ+8PQ1pT8V6DlsK/SxeJ+lBRZk5CSCOOWk/Eiu6d8eNkl4/fzeJCIH9CSCO7cyO6KsmWduxnTwRdR8CiGN7UyPy+D3L0beXJyKvIIA4tjct7BH3XwXCs7YjAIuiuQQQRy7fw1qPXCi1tR320p6W+3Yc1j9+DoGVBBDHdieGre34c2DTYh6C226+iL4ggDi2PxUiy9FZUbr9fNGDxbZpgNg+gcgpy9y7hW2fDj3ojgCfOPpIia3tuNvZFa51OEFRLI8A4shjG2058moF7rBE6VK+KQHE0RTnRo1FVpSyrmMj1FTelADi2JRg2/qR3cLY3Lgte1oLEEAcAVgzFI1c6xjynaMzMCTEDAQQxwyQgyG8G/7YRdJjg21THAJNCCCOJhibNhJZ18FF0qboacxLAHF4Sc1bzi5+et7Hcquk6XWP8/aQaDtNAHH0mf7IrVkeue8zh6V7hTj6TK89w/JPZ9e4SOoERbF2BBBHO5atW/IuQ39Q0vE8NdsaP+0dRABx9Ds/Ihsbf0XSu/odCj2rRgBx9J1R70XSv0h6Rt9DoXeVCCCOvrMZuUjKStK+c1mqd4ij73RGLpKyyU/fuSzVO8TRfzp/LumFzm6STycoim1GgIm2Gb85al8o6XpnoCOSbnCWpRgE1iaAONZGN2vFm50rRL8s6b2z9oxgO0kAcYyRdu9FUl5UPUY+h+8l4hgjhadJOursKjl1gqLY+gSYZOuzm7vm3ySd4AjK3RUHJIpsRgBxbMZvztp2gdQulB52cLpyGCF+vjEBxLExwtka8N5duV/SE2brFYF2kgDiGCftz5P0C2d3L5f0SWdZikEgTABxhJFttcLXJb3N0YPfS3q2oxxFILAWAcSxFratVbpY0jXO6Dy74gRFsTgBxBFnts0atu/GPc4OcHfFCYpicQKII85s2zXulHSqsxOnS/qtsyzFIOAmgDjcqLop6L27Yh3mU0c3aavVEcQxZj69z67Y6MjxmDnuutdMqq7Ts2/nvM+u8KljzPx232vE0X2KVnbQNvixTx1nObr/E0kvdZSjCATcBBCHG1V3Bb2fOtiPtLvUjd8hxDFuDiOvirR3zNq7Zjkg0IQA4miCcWuNfEvSBY7or5B0k6McRSDgIoA4XJi6LeQ9XeG2bLcpHLNjiGPMvE299i5Bt7fCeT6ZjE2D3s9GAHHMhjolkL297VpHy3aaYqcrHBBoQgBxNMG4tUbeJOmbjui/kXSGoxxFIOAigDhcmLotdJKkux29u9W5S7qjKYpAgOXIFebAQ45B2DtoT3aUowgEXAT4xOHC1HUhjzhsDYet5eCAQBMCiKMJxq024hGHdZBcbzVNtYIzmcbPp32aeJJjGOTaAYkiPgJMJh+nnkvZ9YsTHR0k1w5IFPERYDL5OPVc6g5JZzo6SK4dkCjiI8Bk8nHquRTXOHrOTtG+IY7xE4s4xs/hcCNAHMOl7DEd9ojjPkm2+Q8HBJoQQBxNMG6tEe+eHKwc3VqKagZGHGPn1SuO70o6f+yh0vueCCCOnrIR78tlkq50VGM/DgckivgJIA4/qx5LXiXpUkfHLpJ0naMcRSDgIoA4XJi6LXSLpHMcveM9sg5IFPETQBx+Vj2W9K4aZbPiHrM3cJ8Qx8DJk8St2LHzN2zvEcewqZP3jgq3YsfNcbc9RxzdpubQjn1W0kcOLSV9SdIljnIUgYCbAOJwo+quoO01anuOHnZcLel9hxXi5xCIEEAcEVp9lb1T0qmOLr3HuRO6oymKQOARAohjvJng3aB4Ghm3YsfLcfc9Rhzdp+jhDr5B0tmS7GG1j0o6wdntv0t6qrMsxSDgJoA43Ki2VvDmDV5twFLzraWtdmDE0Xd+ve+G3W8URyTd0PcQ6d2IBBBH31n7vqTXrNlFE4aJgwMCzQkgjuZImzZo74W198Ouc5DbdahRx0WAyeXCtLVCp0k6Gox+u6QPS7IH4DggkEIAcaRgbdqoLS3/tKQXLyRy+gGt/1DSK5tGpzEIrCCAOMacFq+T9GZJL5f0gKTfSbpNkl1M5YBAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoRwBx1MspI4JAOgHEkY6YABCoR+D/N5+HPDMCZsAAAAAASUVORK5CYII=';
  //   let filename = 'signature.png';

  //   let file = service.dataURLtoImage(dataurl, filename);

  //   expect(file.name).toEqual(filename);
  // });
});

export class UtilServiceMock {
  async deleteUpload(id: number): Promise<void> {}
  async uploadFile(type: string, form: FormData): Promise<void> {}
  async getUploads(type: string): Promise<Upload[]> {
    return undefined;
  }
  dataURLtoImage(dataurl: string, filename: string): File {
    return undefined;
  }
  jsonToFD(data: Object): FormData {
    return undefined;
  }
  appendImgsToFD(
    form: FormData,
    images: string[],
    image_key: string
  ): FormData {
    return undefined;
  }
  appendImgToFD(form: FormData, image?: string, image_key?: string): FormData {
    return undefined;
  }
  formatPrice(price: number): string {
    return undefined;
  }
  formatDate(date: Date): string {
    return undefined;
  }
}
