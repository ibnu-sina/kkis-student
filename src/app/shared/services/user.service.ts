import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Result } from './rest-interceptor.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user: User;

  constructor(private http: HttpClient) {}

  async getUser(): Promise<User> {
    if (!this.user) {
      try {
        let result: Result = await this.http.get(`/students/me`).toPromise();
        this.user = result.data;
      } catch (error) {}
    }
    return this.user;
  }

  async editUser(data: User): Promise<void> {
    try {
      await this.http.put(`/students`, data).toPromise();
    } catch (error) {
      throw error;
    }
  }
}

export interface User {
  id?: number;
  name?: string;
  ic_no?: string;
  email?: string;
  phone_no?: string;
  gender?: string;
  race?: string;
  religion?: string;
  birth_date?: Date;
  birth_place?: string;
  course?: Course;
  course_id?: number;
  matric_no?: string;
  room?: Room;
  room_status?: string;
  created_at?: Date;
  updated_at?: Date;
}

export interface Course {
  id?: number;
  name?: string;
}

interface Room {
  id?: number;
  name?: string;
  block?: string;
  number?: number;
  capacity?: number;
  occupied?: number;
  description?: string;
  is_available?: boolean;
  created_at?: Date;
  updated_at?: Date;
}
