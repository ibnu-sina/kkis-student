import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { OfflineService } from './offline.service';

describe('OfflineService', () => {
  let service: OfflineService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OfflineService],
    });
    service = TestBed.inject(OfflineService);
  });

  // it('should be created', () => {
  //   expect(service).toBeTruthy();
  // });
});
