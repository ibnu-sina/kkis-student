import { Component, OnInit } from '@angular/core';
import { filter, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import {
  NbSidebarService,
  NbMenuService,
  NbThemeService,
  NbMediaBreakpointsService,
} from '@nebular/theme';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  userMenu = [{ title: 'Settings' }, { title: 'Log out' }];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private authService: AuthService,
    private router: Router,
    public themeService: NbThemeService,
    public breakpointService: NbMediaBreakpointsService
  ) {}

  ngOnInit() {
    this.listenChanges();
  }

  listenChanges() {
    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'user-menu'),
        map(({ item: { title } }) => title)
      )
      .subscribe(async (title) => {
        switch (title) {
          case 'Log out':
            await this.logout();
            break;
          case 'Settings':
            await this.settings();
            break;
        }
      });
  }

  toggle(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  home() {
    this.router.navigate(['']);
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigate(['login']);
  }

  async settings() {
    await this.router.navigate(['settings']);
  }
}
