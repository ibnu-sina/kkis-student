import { Component, HostListener, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-layout',
  template: `
    <nb-layout class="noselect">
      <nb-layout-header fixed>
        <app-header></app-header>
      </nb-layout-header>

      <nb-sidebar tag="menu-sidebar" responsive>
        <nb-menu tag="menu" [items]="menuItems"></nb-menu>
      </nb-sidebar>

      <nb-layout-column class="container">
        <router-outlet></router-outlet>
      </nb-layout-column>
    </nb-layout>
  `,

  styles: [
    `
      .container {
        // background: white;
      }
    `,
  ],
})
export class LayoutComponent implements OnInit {
  innerWidth: number;
  toggleMenu: boolean;
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  constructor() {}

  ngOnInit() {}

  menuItems: NbMenuItem[] = [
    {
      title: 'Home',
      link: '/home',
      icon: 'home-outline',
      home: true,
    },
    {
      title: 'STUDENT',
      group: true,
    },
    {
      title: 'Profile',
      link: '/profile',
      icon: 'person-outline',
    },
    {
      title: 'Merit',
      link: '/merit',
      icon: 'award-outline',
    },
    {
      title: 'FORMS',
      group: true,
    },
    {
      title: 'Stay during Semester Break',
      link: '/stay-during-break',
      icon: 'file-text-outline',
    },
    {
      title: 'Appliances Registration',
      link: '/appliances',
      icon: 'flash-outline',
    },
    {
      title: 'Inventory Storage',
      link: '/inventory-storage',
      icon: 'shopping-bag-outline',
    },
    {
      title: 'SERVICES',
      group: true,
    },
    {
      title: 'Files Manager',
      link: '/file-manager',
      icon: 'folder-outline',
    },
  ];
}
