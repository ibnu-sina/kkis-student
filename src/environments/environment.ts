import { EnvironmentData } from 'src/app/shared/services/environment.service';

export const environment: EnvironmentData = {
  production: false,
  baseUrl: 'https://kkis-api.herokuapp.com/api',
  online: true,
};
