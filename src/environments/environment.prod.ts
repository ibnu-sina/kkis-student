import { EnvironmentData } from 'src/app/shared/services/environment.service';

export const environment: EnvironmentData = {
  production: true,
  baseUrl: 'http://127.0.0.1:3333/api',
  online: false
};
